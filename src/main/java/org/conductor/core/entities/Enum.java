package org.conductor.core.entities;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "enum")
public class Enum {
    @DatabaseField(foreign = true)
    private Property property;

    @DatabaseField(generatedId = true)
    private int id;

    @DatabaseField(canBeNull = false)
    private String value;

    public Enum() {

    }

    public Enum(String value) {
        this.value = value;
    }

    public String getValue() {
        return this.value;
    }
}
