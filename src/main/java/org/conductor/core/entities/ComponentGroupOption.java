package org.conductor.core.entities;

import org.conductor.core.converter.DataTypeConverter;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "componentGroupOption")
public class ComponentGroupOption {
    @DatabaseField(foreign = true)
    private ComponentGroup componentGroup;

    @DatabaseField(generatedId = true)
    private int id;

    @DatabaseField(canBeNull = false)
    private String name;

    @DatabaseField
    private String value;

    @DatabaseField
    private DataType dataType;

    public ComponentGroupOption() {

    }

    public ComponentGroupOption(String name, DataType dataType, Object value) throws Exception {
        this.name = name;
        this.value = value.toString();
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setValue(Object value) {
        this.value = value.toString();
    }

    public Object getValue() throws Exception {
        return DataTypeConverter.getObject(this.dataType, this.value);
    }

    public void setDataType(DataType dataType) {
        this.dataType = dataType;
    }

    public DataType getDataType() {
        return this.dataType;
    }

    public void setComponentGroup(ComponentGroup componentGroup) {
        this.componentGroup = componentGroup;
    }

    public ComponentGroup getComponentGroup() {
        return this.componentGroup;
    }
}
