package org.conductor.core.entities;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "component")
public class Component {
    @DatabaseField(foreign = true)
    private ComponentGroup componentGroup;

    @DatabaseField(id = true)
    private String id;

    @DatabaseField(canBeNull = false)
    private String name;

    @ForeignCollectionField(eager = true)
    private Collection<ComponentOption> options;

    @ForeignCollectionField(eager = true)
    private Collection<Property> properties;

    @ForeignCollectionField(eager = true)
    private Collection<Method> methods;

    public Component() {

    }

    public Component(String id, String name) {
        this(id, name, new ArrayList<>(), new ArrayList<>());
    }

    public Component(String id, String name, Collection<Property> properties, Collection<Method> methods) {
        this.id = id;
        this.name = name;
        this.properties = properties;
        this.methods = methods;
    }

    public void setComponentGroup(ComponentGroup componentGroup) {
        this.componentGroup = componentGroup;
    }

    public ComponentGroup getComponentGroup() {
        return this.componentGroup;
    }

    public void setProperties(Collection<Property> properties) {
        this.properties = properties;
    }

    public Collection<Property> getProperties() {
        return this.properties;
    }

    public void setMethods(Collection<Method> methods) {
        this.methods = methods;
    }

    public Collection<Method> getMethods() {
        return this.methods;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setOptions(Collection<ComponentOption> options) {
        this.options = options;
    }

    public Collection<ComponentOption> getOptions() {
        return this.options;
    }

    public Map<String, Object> getOptionsAsMap() throws Exception {
        Map<String, Object> options = new HashMap<String, Object>();

        for (ComponentOption option : this.options) {
            options.put(option.getName(), option.getValue());
        }

        return options;
    }
}
