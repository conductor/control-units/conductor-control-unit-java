package org.conductor.core.entities;

import java.util.ArrayList;
import java.util.Collection;

import org.conductor.core.converter.DataTypeConverter;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "property")
public class Property {
    @DatabaseField(foreign = true)
    private Component component;

    @DatabaseField(generatedId = true)
    private int id;

    @DatabaseField(canBeNull = false)
    private String name;

    @DatabaseField
    private String value;

    @DatabaseField
    public DataType dataType;

    @DatabaseField
    public boolean isSynced;

    @ForeignCollectionField(eager = true)
    private Collection<Enum> enums;

    public Property() {

    }

    public Property(Component component, String name, Object value) throws Exception {
        this(component, name, value, new ArrayList<>());
    }

    public Property(Component component, String name, Object value, Collection<Enum> enums) throws Exception {
        this.dataType = DataTypeConverter.getDataType(value);
        this.component = component;
        this.name = name;
        this.value = value.toString();
        this.enums = enums;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    public void setValue(Object value) {
        this.value = value.toString();
    }

    public Object getValue() throws Exception {
        return DataTypeConverter.getObject(this.dataType, this.value);
    }

    public void setDataType(DataType dataType) {
        this.dataType = dataType;
    }

    public DataType getDataType() {
        return this.dataType;
    }

    public void setEnum(Collection<Enum> enums) {
        this.enums = enums;
    }

    public Collection<Enum> getEnum() {
        return this.enums;
    }

    public Component getComponent() {
        return this.component;
    }

    public void setComponent(Component component) {
        this.component = component;
    }

    public boolean isSynced() {
        return this.isSynced;
    }

    public void setSynced(boolean isSynced) {
        this.isSynced = isSynced;
    }
}
