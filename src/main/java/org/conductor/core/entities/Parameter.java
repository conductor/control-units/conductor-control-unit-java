package org.conductor.core.entities;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "parameter")
public class Parameter {
    @DatabaseField(foreign = true)
    private Method method;

    @DatabaseField(generatedId = true)
    private int id;

    @DatabaseField(canBeNull = false)
    private String name;

    @DatabaseField(canBeNull = false)
    private DataType dataType;

    public Parameter() {

    }

    public Parameter(Method method, String name, DataType dataType) {
        this.method = method;
        this.name = name;
        this.dataType = dataType;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public DataType getDataType() {
        return this.dataType;
    }

    public void setDataType(DataType dataType) {
        this.dataType = dataType;
    }

    public void setMethod(Method method) {
        this.method = method;
    }
}
