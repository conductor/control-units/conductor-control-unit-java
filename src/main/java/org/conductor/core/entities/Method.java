package org.conductor.core.entities;

import java.util.ArrayList;
import java.util.Collection;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "method")
public class Method {
    @DatabaseField(foreign = true)
    private Component component;

    @DatabaseField(generatedId = true)
    private int id;

    @DatabaseField(canBeNull = false)
    private String name;

    @ForeignCollectionField(eager = true)
    private Collection<Parameter> parameters;

    public Method() {

    }

    public Method(Component component, String name) {
        this(component, name, new ArrayList<>());
    }

    public Method(Component component, String name, Collection<Parameter> parameters) {
        this.component = component;
        this.name = name;
        this.parameters = parameters;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Collection<Parameter> getParameters() {
        return this.parameters;
    }

    public void setParameters(Collection<Parameter> parameters) {
        this.parameters = parameters;
    }

    public void call(Collection<Parameter> parameters) {

    }

    public void setComponent(Component component) {
        this.component = component;
    }

    public Component getComponent() {
        return this.component;
    }
}
