package org.conductor.core.entities;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.conductor.core.communicator.interfaces.CommunicationChannel;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "componentGroup")
public class ComponentGroup {
    @DatabaseField(canBeNull = false, id = true)
    private String id;

    @DatabaseField(canBeNull = false)
    private String org;

    @DatabaseField(canBeNull = false)
    private String name;

    @DatabaseField(canBeNull = false)
    private String version;

    @DatabaseField(canBeNull = false)
    private String repositoryUrl;

    @DatabaseField(canBeNull = false)
    private String installationRequestId;

    @ForeignCollectionField(eager = true)
    private Collection<Component> components;

    @ForeignCollectionField(eager = true)
    private Collection<ComponentGroupOption> options;

    private CommunicationChannel componentGroupCommunicator;

    public ComponentGroup() {

    }

    public ComponentGroup(String org, String name, String repositoryUri, List<Component> components, List<ComponentGroupOption> options) {
        this.org = org;
        this.name = name;
        this.repositoryUrl = repositoryUri;
        this.components = components;
        this.options = options;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return this.id;
    }

    public String getOrg() {
        return this.org;
    }

    public void setOrg(String org) {
        this.org = org;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRepositoryUrl() {
        return this.repositoryUrl;
    }

    public void setRepositoryUrl(String repositoryUrl) {
        this.repositoryUrl = repositoryUrl;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getVersion() {
        return this.version;
    }

    public Collection<Component> getComponents() {
        return this.components;
    }

    public void setComponents(Collection<Component> components) {
        this.components = components;
    }

    public Collection<ComponentGroupOption> getOptions() {
        return this.options;
    }

    public Map<String, Object> getOptionsAsMap() throws Exception {
        Map<String, Object> options = new HashMap<String, Object>();

        for (ComponentGroupOption option : this.options) {
            options.put(option.getName(), option.getValue());
        }

        return options;
    }

    public void setOptions(Collection<ComponentGroupOption> options) {
        this.options = options;
    }

    public void setCommunicator(CommunicationChannel communicator) {
        this.componentGroupCommunicator = communicator;
    }

    public CommunicationChannel getCommunicator() {
        return this.componentGroupCommunicator;
    }

    @Override
    public String toString() {
        return this.org + ":" + this.name + ":" + this.version;
    }

    public String getInstallationRequestId() {
        return installationRequestId;
    }

    public void setInstallationRequestId(String installationRequestId) {
        this.installationRequestId = installationRequestId;
    }
}