package org.conductor.core.entities;

public enum DataType {
    INTEGER("integer"),
    DOUBLE("double"),
    FLOAT("float"),
    BOOLEAN("boolean"),
    STRING("string"),
    ENUM("enum");

    private final String text;

    DataType(final String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return text;
    }
}