package org.conductor.core.validation;

public class ValidationResult {
    private boolean isValid;
    private String message;
    private ValidationResult childValidationResult;

    public ValidationResult(boolean isValid) {
        this(isValid, null);
    }

    public ValidationResult(boolean isValid, String message) {
        this.isValid = isValid;
        this.message = message;
    }

    public ValidationResult(boolean isValid, String message, ValidationResult childValidationResult) {
        this.isValid = isValid;
        this.message = message;
        this.childValidationResult = childValidationResult;
    }

    public void setValid(boolean isValid) {
        this.isValid = isValid;
    }

    public boolean isValid() {
        return this.isValid;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return this.message;
    }

    public void setChildValidationResult(ValidationResult validation) {
        this.childValidationResult = validation;
    }

    public ValidationResult getChildValidationResult() {
        return this.childValidationResult;
    }

    @Override
    public String toString() {
        return getMessage() + " " + this.childValidationResult;
    }

}
