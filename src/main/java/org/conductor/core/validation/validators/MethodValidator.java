package org.conductor.core.validation.validators;

import org.conductor.core.entities.Method;
import org.conductor.core.entities.Parameter;
import org.conductor.core.validation.ValidationResult;

public class MethodValidator {
    public static ValidationResult validate(Method method) {
        if (method.getName() == null || method.getName().isEmpty()) {
            return new ValidationResult(false, "Invalid method name. The name of the method cannot be empty.");
        }

        for (Parameter parameter : method.getParameters()) {
            if (parameter.getName() == null || parameter.getName().isEmpty()) {
                return new ValidationResult(false, "Invalid parameter name in the method '" + method.getName() + "'. The name of the parameter cannot be empty.");
            }

            if (parameter.getDataType() == null) {
                return new ValidationResult(false, "The method '" + method.getName() + "' contains a parameter with the name '" + parameter.getName() + "' that has a data type field that is null.");
            }
        }

        return new ValidationResult(true);
    }
}
