package org.conductor.core.validation.validators;

import java.net.URI;
import java.net.URISyntaxException;

import org.conductor.core.entities.Component;
import org.conductor.core.entities.ComponentGroup;
import org.conductor.core.entities.ComponentGroupOption;
import org.conductor.core.validation.ValidationResult;

public class ComponentGroupValidator {
    public static ValidationResult validate(ComponentGroup componentGroup) {
        ValidationResult childValidation;

        if (componentGroup == null) {
            return new ValidationResult(false, "Invalid component group. Component group cannot be null");
        }

        try {
            new URI(componentGroup.getRepositoryUrl());
        } catch (URISyntaxException e) {
            return new ValidationResult(false, "Invalid repository url in component group '" + componentGroup + "'");
        }

        if (componentGroup.getId() == null || componentGroup.getId().isEmpty()) {
            return new ValidationResult(false, "Invalid component group id in component group '" + componentGroup + "'. The id cannot be empty.");
        }

        if (componentGroup.getName() == null || componentGroup.getName().isEmpty()) {
            return new ValidationResult(false, "Invalid component group name in component group '" + componentGroup + "'. The name cannot be empty.");
        }

        if (componentGroup.getOrg() == null || componentGroup.getOrg().isEmpty()) {
            return new ValidationResult(false, "Invalid component group org in component group v. The org value cannot be empty.");
        }

        if (componentGroup.getVersion() == null || componentGroup.getVersion().isEmpty()) {
            return new ValidationResult(false, "Invalid component group version in component group '" + componentGroup + "'. The version of the component group cannot be empty.");
        }

        if (componentGroup.getOptions() == null) {
            return new ValidationResult(false, "Invalid component group options in component group '" + componentGroup + "'. The options cannot be null.");
        }

        for (ComponentGroupOption option : componentGroup.getOptions()) {
            if (option.getName() == null || option.getName().isEmpty()) {
                return new ValidationResult(false, "Invalid component group option in component group '" + componentGroup + "'. The component group cannot contain an option with an empty name.");
            }

            if (option.getDataType() == null) {
                return new ValidationResult(false, "Invalid component group option in component group '" + componentGroup + "'. The data type field cannot be empty or null.");
            }

            Object optionValue;

            try {
                optionValue = option.getValue();
            } catch (Exception e) {
                return new ValidationResult(false, "Invalid component group option in component group '" + componentGroup + "'. " + e.getMessage());
            }

            if (optionValue == null) {
                return new ValidationResult(false, "Invalid component group option in component group '" + componentGroup + "'. The value of the option '" + option.getName() + "' is null.");
            }

            childValidation = ValueValidator.validate(option.getDataType(), optionValue);
            if (!childValidation.isValid()) {
                return new ValidationResult(false, "Invalid component group option '" + option.getName() + "' in component group '" + componentGroup + "'.", childValidation);
            }
        }

        for (Component component : componentGroup.getComponents()) {
            childValidation = ComponentValidator.validate(component);
            if (!childValidation.isValid()) {
                return new ValidationResult(false, "Invalid component in component group '" + componentGroup + "'.", childValidation);
            }
        }

        return new ValidationResult(true);
    }
}
