package org.conductor.core.validation.validators;

import org.conductor.core.entities.Property;
import org.conductor.core.validation.ValidationResult;

public class PropertyValidator {
    public static ValidationResult validate(Property property) {
        if (property.getName() == null || property.getName().isEmpty()) {
            return new ValidationResult(false, "Invalid property name. The name of the property cannot be empty.");
        }

        Object propertyValue;

        try {
            propertyValue = property.getValue();
        } catch (Exception e) {
            return new ValidationResult(false, "The property '" + property.getName() + "' contains an invalid value. " + e.getMessage());
        }

        if (propertyValue == null) {
            return new ValidationResult(false, "The property '" + property.getName() + "' contains an invalid value. The value of the property cannot be null.");
        }

        if (property.getDataType() == null) {
            return new ValidationResult(false, "The property '" + property.getName() + "' contains an invalid data type. The data type field on the property cannot be null.");
        }

        if (property.getEnum() == null) {
            return new ValidationResult(false, "The property '" + property.getName() + "' contains an invalid enum. The enum cannot be null, it should atleast be an empty collection.");
        }

        ValidationResult childValidation = ValueValidator.validate(property.getDataType(), propertyValue, property.getEnum());
        if (!childValidation.isValid()) {
            return new ValidationResult(false, "Invalid property value in property '" + property.getName() + "'.", childValidation);
        }

        return new ValidationResult(true);
    }
}
