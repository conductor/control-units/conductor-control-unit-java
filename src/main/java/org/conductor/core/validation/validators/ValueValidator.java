package org.conductor.core.validation.validators;

import java.util.Collection;

import org.conductor.core.entities.DataType;
import org.conductor.core.entities.Enum;
import org.conductor.core.validation.ValidationResult;

public class ValueValidator {
    public static ValidationResult validate(DataType dataType, Object value) {
        boolean isValid;

        switch (dataType) {
            case STRING:
                isValid = value instanceof String;
                break;
            case INTEGER:
                isValid = value instanceof Integer;
                break;
            case FLOAT:
                isValid = value instanceof Float;
                break;
            case DOUBLE:
                isValid = value instanceof Double;
                break;
            case BOOLEAN:
                isValid = value instanceof Boolean;
                break;
            default:
                return new ValidationResult(false, "Invalid data type. Cannot validate data type: '" + dataType + "'.");
        }

        if (isValid) {
            return new ValidationResult(true);
        } else {
            return new ValidationResult(false, "Invalid data type. Expected the data type of the value to be '" + dataType + "', but the value was of data type '" + value.getClass().getSimpleName() + "'");
        }
    }

    public static ValidationResult validate(DataType dataType, Object value, Collection<Enum> enums) {
        boolean isValid = false;

        switch (dataType) {
            case STRING:
                isValid = value instanceof String;
                break;
            case INTEGER:
                isValid = value instanceof Integer;
                break;
            case FLOAT:
                isValid = value instanceof Float;
                break;
            case DOUBLE:
                isValid = value instanceof Double;
                break;
            case BOOLEAN:
                isValid = value instanceof Boolean;
                break;
            case ENUM:
                boolean found = false;
                for (Enum enumValue : enums) {
                    if (enumValue.getValue().equals(value)) {
                        found = true;
                    }
                }

                if (found) {
                    isValid = true;
                } else {
                    return new ValidationResult(false, "Couldn't find the value '" + value + "' in enum");
                }

                break;
            default:
                return new ValidationResult(false, "Invalid data type. Cannot validate data type: '" + dataType + "'.");
        }

        if (isValid) {
            return new ValidationResult(true);
        } else {
            return new ValidationResult(false, "Invalid data type. Expected the data type of the value to be '" + dataType + "', but the value was of data type '" + value.getClass().getSimpleName() + "'");
        }
    }
}
