package org.conductor.core.validation.validators;

import org.conductor.core.entities.Component;
import org.conductor.core.entities.ComponentOption;
import org.conductor.core.entities.Method;
import org.conductor.core.entities.Property;
import org.conductor.core.validation.ValidationResult;

public class ComponentValidator {
    public static ValidationResult validate(Component component) {
        ValidationResult childValidation;

        if (component.getName() == null || component.getName().isEmpty()) {
            return new ValidationResult(false, "Invalid component. The name of the component cannot be empty.");
        }

        if (component.getId() == null || component.getId().isEmpty()) {
            return new ValidationResult(false, "The component with the name '" + component.getName() + "' contains an invalid id. The id cannot be empty.");
        }

        for (ComponentOption option : component.getOptions()) {
            if (option.getName() == null || option.getName().isEmpty()) {
                return new ValidationResult(false, "Invalid component option in group '" + component.getName() + "'. The component cannot contain an option with an empty name.");
            }

            if (option.getDataType() == null) {
                return new ValidationResult(false, "Invalid component option in component '" + component.getName() + "'. The data type field cannot be empty or null.");
            }

            Object optionValue;

            try {
                optionValue = option.getValue();
            } catch (Exception e) {
                return new ValidationResult(false, "Invalid component option '" + option.getName() + "' in component '" + component.getName() + "'. " + e.getMessage());
            }

            if (optionValue == null) {
                return new ValidationResult(false, "Invalid component option in component '" + component.getName() + "'. The value of the option '" + option.getName() + "' is null.");
            }

            childValidation = ValueValidator.validate(option.getDataType(), optionValue);
            if (!childValidation.isValid()) {
                return new ValidationResult(false, "Invalid component option in component '" + component.getName() + "'. ", childValidation);
            }
        }

        for (Property property : component.getProperties()) {
            childValidation = PropertyValidator.validate(property);
            if (!childValidation.isValid()) {
                return new ValidationResult(false, "Invalid component property in component '" + component.getName() + "'.", childValidation);
            }
        }

        for (Method method : component.getMethods()) {
            childValidation = MethodValidator.validate(method);
            if (!childValidation.isValid()) {
                return new ValidationResult(false, "Invalid component method in component '" + component.getName() + "'.", childValidation);
            }
        }

        return new ValidationResult(true);
    }
}
