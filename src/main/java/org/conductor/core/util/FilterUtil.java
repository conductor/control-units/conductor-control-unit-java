package org.conductor.core.util;

import java.util.Collection;

import org.conductor.core.entities.Component;
import org.conductor.core.entities.ComponentGroup;
import org.conductor.core.entities.Method;
import org.conductor.core.entities.Property;

public class FilterUtil {

    public static ComponentGroup getComponentGroup(Collection<ComponentGroup> componentGroups, String componentGroupId) {
        for (ComponentGroup componentGroup : componentGroups) {
            if (componentGroup.getId().equals(componentGroupId)) {
                return componentGroup;
            }
        }

        return null;
    }

    public static Component getComponent(Collection<ComponentGroup> componentGroups, String componentId) {
        for (ComponentGroup componentGroup : componentGroups) {
            for (Component component : componentGroup.getComponents()) {
                if (component.getId().equals(componentId)) {
                    return component;
                }
            }
        }

        return null;
    }

    public static Property getProperty(Collection<ComponentGroup> componentGroups, String componentId, String propertyName) {
        Component component = getComponent(componentGroups, componentId);

        if (component == null) {
            return null;
        }

        for (Property property : component.getProperties()) {
            if (property.getName().equals(propertyName)) {
                return property;
            }
        }

        return null;
    }

    public static Method getMethod(Collection<ComponentGroup> componentGroups, String componentId, String methodName) {
        Component component = getComponent(componentGroups, componentId);

        if (component == null) {
            return null;
        }

        for (Method method : component.getMethods()) {
            if (method.getName().equals(methodName)) {
                return method;
            }
        }

        return null;
    }

}
