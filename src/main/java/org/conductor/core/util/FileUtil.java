package org.conductor.core.util;

public class FileUtil {
    public static String getSafeFileName(String fileName) {
        return fileName.replaceAll("[^a-zA-Z0-9.-]", "_");
    }
}
