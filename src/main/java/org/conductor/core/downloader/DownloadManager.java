package org.conductor.core.downloader;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.conductor.core.downloader.listeners.DownloadListener;
import org.conductor.core.downloader.listeners.DownloadManagerListener;
import org.conductor.core.entities.ComponentGroup;
import org.conductor.core.util.FileUtil;
import org.springframework.stereotype.Component;

@Component
public class DownloadManager implements DownloadListener {
    private static Logger log = LogManager.getLogger(DownloadManager.class.getName());
    private Map<Download, String> downloads = new HashMap<>();
    private List<ComponentGroup> componentGroups = new ArrayList<>();
    private DownloadFactory downloadFactory;
    private List<DownloadManagerListener> eventListeners = new ArrayList<>();
    private File repositoryFolder;

    public DownloadManager() {
        this.repositoryFolder = new File("./repository");
        File tempDownloadFolder = new File("./repository/temp");

        tempDownloadFolder.mkdir();
        this.repositoryFolder.mkdir();
        this.downloadFactory = new DownloadFactory(tempDownloadFolder);
    }

    public void download(ComponentGroup componentGroup) throws MalformedURLException {
        if (componentGroups.contains(componentGroup)) {
            log.info("A download already exist for component group '" + componentGroup + "'.");
            return;
        }

        boolean found = false;

        for (String compactGroupName : downloads.values()) {
            if (compactGroupName.equals(componentGroup)) {
                found = true;
            }
        }

        if (found) {
            log.info("A download already exist for component group '" + componentGroup + "'.");
        } else {
            Download download = this.downloadFactory.create(getDownloadURL(componentGroup), getSaveFilePath(componentGroup));

            downloads.put(download, componentGroup.toString());
            componentGroups.add(componentGroup);

            download.addStatusChangedListener(this);
            download.start();
        }
    }

    private URL getDownloadURL(ComponentGroup componentGroup) throws MalformedURLException {
        return new URL(componentGroup.getRepositoryUrl() + "/download/" + componentGroup.getOrg() + "/" + componentGroup.getName() + "/" + componentGroup.getVersion());
    }

    private File getSaveFilePath(ComponentGroup componentGroup) throws MalformedURLException {
        String repositoryUri = FileUtil.getSafeFileName(getDownloadURL(componentGroup).getHost());
        String org = FileUtil.getSafeFileName(componentGroup.getOrg());
        String name = FileUtil.getSafeFileName(componentGroup.getName());
        String version = FileUtil.getSafeFileName(componentGroup.getVersion());
        return new File(this.repositoryFolder.getAbsolutePath() + "/" + repositoryUri + "/" + org + "/" + name + "/" + version + "/" + name + "-" + version + ".jar");
    }

    @Override
    public void onStarted(Download download) {
        log.info("Started downloading '" + downloads.get(download) + "'.");
    }

    @Override
    public void onFinished(Download download) {
        List<ComponentGroup> componentGroups = getComponentGroupsByDownload(download);
        log.info("Finished downloading '" + downloads.get(download) + "'.");
        downloads.remove(download);

        for (ComponentGroup componentGroup : componentGroups) {
            for (DownloadManagerListener listener : eventListeners) {
                listener.onDownloadFinished(componentGroup);
            }
        }
    }

    @Override
    public void onError(Download download, Exception e) {
        List<ComponentGroup> componentGroups = getComponentGroupsByDownload(download);
        log.info("An error occured while downloading '" + downloads.get(download) + "' the error was: '" + e.getMessage() + "'.");

        for (ComponentGroup componentGroup : componentGroups) {
            for (DownloadManagerListener listener : eventListeners) {
                listener.onDownloadError(componentGroup, e);
            }
        }
    }

    @Override
    public void onCanceled(Download download) {
        log.info("Canceled downloading of '" + downloads.get(download) + "'.");
    }

    public List<ComponentGroup> getComponentGroupsByDownload(Download download) {
        List<ComponentGroup> retComponentGroups = new ArrayList<ComponentGroup>();
        String compactGroupName = downloads.get(download);

        for (ComponentGroup componentGroup : componentGroups) {
            if (compactGroupName.equals(componentGroup.toString())) {
                retComponentGroups.add(componentGroup);
            }
        }

        return retComponentGroups;
    }

    public void addEventListener(DownloadManagerListener listener) {
        this.eventListeners.add(listener);
    }

    public void removeEventListener(DownloadManagerListener listener) {
        this.eventListeners.remove(listener);
    }
}