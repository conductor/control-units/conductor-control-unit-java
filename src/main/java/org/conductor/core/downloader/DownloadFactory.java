package org.conductor.core.downloader;

import java.io.File;
import java.net.URL;

public class DownloadFactory {
    private File tempPath;

    public DownloadFactory(File tempPath) {
        this.tempPath = tempPath;
    }

    public Download create(URL uri, File saveFile) {
        return new Download(uri, saveFile.getName(), saveFile.getParentFile(), this.tempPath);
    }
}
