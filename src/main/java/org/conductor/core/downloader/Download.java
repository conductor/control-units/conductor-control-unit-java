package org.conductor.core.downloader;

import java.io.File;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.conductor.core.downloader.listeners.DownloadListener;

public class Download implements Runnable {
    private URL url;
    private Status status;
    private double downloaded = 0;
    private double size = -1;
    private static int MAX_BUFFER_SIZE = 1024;
    private File finishedFilePath;
    private File tempFilePath;
    private List<DownloadListener> eventListeners = new ArrayList<DownloadListener>();

    public Download(URL url, String fileName, File savePath, File tempSavePath) {
        this.url = url;

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd H;s.S");
        this.finishedFilePath = new File(savePath.getAbsolutePath() + File.separator + fileName);
        this.tempFilePath = new File(tempSavePath.getAbsolutePath() + File.separator + format.format(new Date()) + " " + fileName);
        this.finishedFilePath.getParentFile().mkdirs();
        this.tempFilePath.getParentFile().mkdirs();
    }

    public void start() {
        Thread thread = new Thread(this);
        thread.start();
    }

    public void cancel() {
        this.status = Status.CANCELED;
    }

    public void run() {
        RandomAccessFile file = null;
        InputStream stream = null;

        try {
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            HttpURLConnection.setFollowRedirects(true);

            connection.setRequestMethod("POST");
            connection.setRequestProperty("Range", "bytes=0-");
            connection.setInstanceFollowRedirects(true);
            connection.connect();

            if (connection.getResponseCode() / 100 != 2) {
                throw new Exception("Server responded with code: " + connection.getResponseCode());
            }

            // Check for valid content length.
            int contentLength = connection.getContentLength();
            if (contentLength < 1) {
                throw new Exception("Server responded with an invalid content length.");
            }

            size = contentLength;
            file = new RandomAccessFile(this.tempFilePath, "rw");
            stream = connection.getInputStream();
            changeStatus(Status.DOWNLOADING);
            while (isStatus(Status.DOWNLOADING)) {
                byte buffer[];
                if (size - downloaded > MAX_BUFFER_SIZE) {
                    buffer = new byte[MAX_BUFFER_SIZE];
                } else {
                    buffer = new byte[(int) (size - downloaded)];
                }

                // Read from server into buffer.
                int read = stream.read(buffer);
                if (read == -1)
                    break;

                // Write buffer to file.
                file.write(buffer, 0, read);
                downloaded += read;
            }
        } catch (Exception e) {
            reportError(e);
            changeStatus(Status.ERROR);
        } finally {
            if (file != null) {
                try {
                    file.close();
                } catch (Exception e) {
                }
            }

            if (stream != null) {
                try {
                    stream.close();
                } catch (Exception e) {
                }
            }
        }

        if (isStatus(Status.DOWNLOADING)) {
            moveFile(tempFilePath, finishedFilePath);
            changeStatus(Status.FINISHED);
        }
    }

    public boolean isStatus(Status status) {
        return this.status == status;
    }

    private void changeStatus(Status status) {
        this.status = status;

        switch (status) {
            case DOWNLOADING:
                for (DownloadListener eventListener : this.eventListeners) {
                    eventListener.onStarted(this);
                }
                break;
            case FINISHED:
                for (DownloadListener eventListener : this.eventListeners) {
                    eventListener.onFinished(this);
                }
                break;
            case CANCELED:
                for (DownloadListener eventListener : this.eventListeners) {
                    eventListener.onCanceled(this);
                }
                break;
            default:
                break;
        }
    }

    public Status getStatus() {
        return this.status;
    }

    private void moveFile(File from, File to) {
        from.renameTo(to);
    }

    private void reportError(Exception e) {
        this.status = Status.ERROR;

        for (DownloadListener eventListener : this.eventListeners) {
            eventListener.onError(this, e);
        }
    }

    public void addStatusChangedListener(DownloadListener eventListener) {
        this.eventListeners.add(eventListener);
    }

    public void removeStatusChangedListener(DownloadListener eventListener) {
        this.eventListeners.remove(eventListener);
    }
}
