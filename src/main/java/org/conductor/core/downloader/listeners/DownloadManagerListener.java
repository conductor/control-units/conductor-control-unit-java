package org.conductor.core.downloader.listeners;

import org.conductor.core.entities.ComponentGroup;

public interface DownloadManagerListener {
    void onDownloadFinished(ComponentGroup componentGroup);

    void onDownloadError(ComponentGroup componentGroup, Exception e);
}
