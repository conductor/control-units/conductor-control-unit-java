package org.conductor.core.downloader.listeners;

import org.conductor.core.downloader.Download;

public interface DownloadListener {
    void onStarted(Download download);

    void onFinished(Download download);

    void onError(Download download, Exception e);

    void onCanceled(Download download);
}
