package org.conductor.core.downloader;

public enum Status {
    DOWNLOADING,
    FINISHED,
    ERROR,
    CANCELED
}
