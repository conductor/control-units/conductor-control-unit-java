package org.conductor.core.communicator.listeners;

import org.conductor.core.entities.Component;
import org.conductor.core.entities.ComponentGroup;

public interface ErrorListener {
    void onComponentError(Component component, String errorMessage, String stackTrace);

    void onComponentGroupError(ComponentGroup componentGroup, String errorMessage, String stackTrace);
}
