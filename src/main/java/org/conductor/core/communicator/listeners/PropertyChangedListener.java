package org.conductor.core.communicator.listeners;

import org.conductor.core.entities.Property;

public interface PropertyChangedListener {
    void onPropertyChanged(Property property, Object propertyValue);
}
