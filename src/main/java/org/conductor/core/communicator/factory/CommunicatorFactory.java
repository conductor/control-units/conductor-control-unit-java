package org.conductor.core.communicator.factory;

import java.io.File;
import java.net.URL;
import java.net.URLClassLoader;

import javax.inject.Inject;

import org.conductor.core.communicator.ComponentGroupCommunicator;
import org.conductor.core.communicator.interfaces.CommunicationChannel;
import org.conductor.core.entities.ComponentGroup;
import org.conductor.core.repository.ComponentGroupImplementationRepository;
import org.springframework.stereotype.Component;

@Component
public class CommunicatorFactory {
    private ComponentGroupImplementationRepository repository;

    @Inject
    public CommunicatorFactory(ComponentGroupImplementationRepository repository) {
        this.repository = repository;
    }

    public CommunicationChannel createCommunicator(ComponentGroup componentGroup) throws Exception {
        File file = repository.getFile(componentGroup);
        URLClassLoader loader = new JarSeekingURLClassLoader(file);
        Class cls = Class.forName("org.conductor.integration.ClientIntegration", true, loader);

        return new ComponentGroupCommunicator(componentGroup, cls, loader);
    }

}
