package org.conductor.core.communicator.interfaces;

import org.conductor.core.communicator.listeners.ErrorListener;
import org.conductor.core.communicator.listeners.PropertyChangedListener;
import org.conductor.core.entities.Method;
import org.conductor.core.entities.Property;

import java.lang.reflect.InvocationTargetException;
import java.util.Map;

public interface CommunicationChannel {
    void start() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, Exception;

    void setPropertyValue(Property property, Object propertyValue) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException;

    void callMethod(Method method, Map<String, Object> parameters) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException;

    void addPropertyChangedListener(PropertyChangedListener listener);

    void removePropertyChangedListener(PropertyChangedListener listener);

    void addErrorListener(ErrorListener listener);

    void removeErrorListener(ErrorListener listener);

    void destroy() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException;
}
