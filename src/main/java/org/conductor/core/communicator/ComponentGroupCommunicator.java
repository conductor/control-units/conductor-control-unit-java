package org.conductor.core.communicator;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.conductor.core.communicator.interfaces.CommunicationChannel;
import org.conductor.core.communicator.listeners.ErrorListener;
import org.conductor.core.communicator.listeners.PropertyChangedListener;
import org.conductor.core.entities.Component;
import org.conductor.core.entities.ComponentGroup;
import org.conductor.core.entities.Property;

public class ComponentGroupCommunicator implements CommunicationChannel {
    private static Logger log = LogManager.getLogger(ComponentGroupCommunicator.class.getName());
    private ComponentGroup componentGroup;
    private Object integrationProxy;
    private java.lang.reflect.Method setPropertyValue;
    private java.lang.reflect.Method setCurrentPropertyValue;
    private java.lang.reflect.Method callMethod;
    private java.lang.reflect.Method start;
    private java.lang.reflect.Method setComponentOptions;
    private java.lang.reflect.Method destroy;
    private java.lang.reflect.Method setClassLoader;
    private List<PropertyChangedListener> propertyChangedListeners = new ArrayList<PropertyChangedListener>();
    private List<ErrorListener> errorListeners = new ArrayList<ErrorListener>();

    public ComponentGroupCommunicator(ComponentGroup componentGroup, Class integrationProxy, URLClassLoader loader) throws Exception {
        log.info("Initializing component group '{}'", componentGroup);
        this.componentGroup = componentGroup;
        java.lang.reflect.Method onPropertyValueChanged = this.getClass().getMethod("onPropertyValueChanged", String.class, String.class, Object.class);
        java.lang.reflect.Method reportError = this.getClass().getMethod("onErrorReported", String.class, String.class, String.class);

        this.integrationProxy = integrationProxy.getDeclaredConstructor(Object.class, Method.class, Method.class).newInstance(this, onPropertyValueChanged, reportError);
        this.setPropertyValue = integrationProxy.getMethod("setPropertyValue", String.class, String.class, Object.class);
        this.callMethod = integrationProxy.getMethod("callMethod", String.class, String.class, java.util.Map.class);
        this.setCurrentPropertyValue = integrationProxy.getMethod("setInitialPropertyValue", String.class, String.class, Object.class);
        this.start = integrationProxy.getMethod("start", Map.class);
        this.setComponentOptions = integrationProxy.getMethod("setComponentOptions", String.class, Map.class);
        this.destroy = integrationProxy.getMethod("destroy");
        this.setClassLoader = integrationProxy.getMethod("setClassLoader", ClassLoader.class);

        setClassLoader(loader);

        for (Component component : componentGroup.getComponents()) {
            setComponentOptions(component.getName(), component.getOptionsAsMap());

            for (Property property : component.getProperties()) {
                setInitialPropertyValue(component.getName(), property.getName(), property.getValue());
            }
        }

        log.info("Initialization completed for component group '{}'", componentGroup);
    }

    public void onErrorReported(String componentName, String errorMessage, String stackTrace) throws Exception {
        Component component = null;

        for (Component tempComponent : componentGroup.getComponents()) {
            if (tempComponent.getName().equals(componentName)) {
                component = tempComponent;
            }
        }

        if (component == null) {
            throw new Exception("Unable to report error, no component exists with the name '" + componentName + "'");
        }

        for (ErrorListener listener : errorListeners) {
            listener.onComponentError(component, errorMessage, stackTrace);
        }
    }

    public void onPropertyValueChanged(String componentName, String propertyName, Object propertyValue) {
        log.info("Property updated in component group '{}' in component '{}' in property '{}' with value '{}'", this.componentGroup, componentName, propertyName, propertyValue);

        Property property = null;
        for (Component component : this.componentGroup.getComponents()) {
            if (component.getName().equals(componentName)) {
                for (Property tempProperty : component.getProperties()) {
                    if (tempProperty.getName().equals(propertyName)) {
                        property = tempProperty;
                    }
                }
            }
        }

        for (PropertyChangedListener propertyChangedListener : propertyChangedListeners) {
            propertyChangedListener.onPropertyChanged(property, propertyValue);
        }
    }

    @Override
    public void start() throws Exception {
        log.info("Starting component group '{}'.", this.componentGroup);
        this.start.invoke(this.integrationProxy, this.componentGroup.getOptionsAsMap());
    }

    @Override
    public void setPropertyValue(Property property, Object propertyValue) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        this.setPropertyValue.invoke(this.integrationProxy, property.getComponent().getName(), property.getName(), propertyValue);
    }

    @Override
    public void callMethod(org.conductor.core.entities.Method method, Map<String, Object> parameters) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        this.callMethod.invoke(this.integrationProxy, method.getComponent(), method.getName(), parameters);
    }

    private void setInitialPropertyValue(String componentName, String propertyName, Object propertyValue) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        this.setCurrentPropertyValue.invoke(this.integrationProxy, componentName, propertyName, propertyValue);
    }

    private void setComponentOptions(String componentName, Map<String, Object> options) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        this.setComponentOptions.invoke(this.integrationProxy, componentName, options);
    }

    private void setClassLoader(ClassLoader loader) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        this.setClassLoader.invoke(this.integrationProxy, loader);
    }

    @Override
    public void destroy() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        log.info("Destroying component group communication for component group '{}'", this.componentGroup);
        propertyChangedListeners.clear();
        this.destroy.invoke(this.integrationProxy);
    }

    @Override
    public void addPropertyChangedListener(PropertyChangedListener listener) {
        this.propertyChangedListeners.add(listener);
    }

    @Override
    public void removePropertyChangedListener(PropertyChangedListener listener) {
        this.propertyChangedListeners.remove(listener);
    }

    @Override
    public void addErrorListener(ErrorListener listener) {
        this.errorListeners.add(listener);
    }

    @Override
    public void removeErrorListener(ErrorListener listener) {
        this.errorListeners.remove(listener);
    }

}
