package org.conductor.core.repository;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;

import org.conductor.core.entities.ComponentGroup;
import org.conductor.core.util.FileUtil;
import org.springframework.stereotype.Component;

@Component
public class ComponentGroupImplementationRepository {
    private File repositoryRootFolder = new File("./repository");

    public boolean exist(ComponentGroup componentGroup) throws MalformedURLException {
        return getFile(componentGroup).exists();
    }

    public File getFile(ComponentGroup componentGroup) throws MalformedURLException {
        String repositoryUri = FileUtil.getSafeFileName(new URL(componentGroup.getRepositoryUrl()).getHost());
        String org = FileUtil.getSafeFileName(componentGroup.getOrg());
        String name = FileUtil.getSafeFileName(componentGroup.getName());
        String version = FileUtil.getSafeFileName(componentGroup.getVersion());

        String path = repositoryRootFolder.getAbsolutePath() + File.separator + repositoryUri + File.separator + org + File.separator + name + File.separator + version + File.separator + name + "-" + version + ".jar";
        return new File(path);
    }
}
