package org.conductor.core.repository;

import java.sql.SQLException;
import java.util.Collection;
import java.util.List;

import org.conductor.core.entities.Component;
import org.conductor.core.entities.ComponentGroup;
import org.conductor.core.entities.ComponentGroupOption;
import org.conductor.core.entities.ComponentOption;
import org.conductor.core.entities.Enum;
import org.conductor.core.entities.Method;
import org.conductor.core.entities.Parameter;
import org.conductor.core.entities.Property;
import org.conductor.integration.client.Collections;
import org.springframework.stereotype.Repository;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.jdbc.JdbcConnectionSource;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.support.DatabaseConnection;
import com.j256.ormlite.table.TableUtils;

@Repository
public class ComponentGroupRepository {
    private final String databaseUrl = "jdbc:sqlite:conductor.db";
    private ConnectionSource connectionSource;
    private DatabaseConnection databaseConnection;
    private Dao<ComponentGroup, String> componentGroupDAO;
    private Dao<ComponentGroupOption, String> componentGroupOptionDAO;
    private Dao<Component, String> componentDAO;
    private Dao<ComponentOption, String> componentOptionDAO;
    private Dao<Enum, String> enumDAO;
    private Dao<Method, String> methodDAO;
    private Dao<Parameter, String> parameterDAO;
    private Dao<Property, String> propertyDAO;

    public ComponentGroupRepository() throws SQLException {
        this.connectionSource = new JdbcConnectionSource(this.databaseUrl);
        this.databaseConnection = this.connectionSource.getReadWriteConnection();
        this.databaseConnection.setAutoCommit(false);

        componentGroupDAO = DaoManager.createDao(this.connectionSource, ComponentGroup.class);
        componentGroupOptionDAO = DaoManager.createDao(this.connectionSource, ComponentGroupOption.class);
        componentDAO = DaoManager.createDao(this.connectionSource, Component.class);
        componentOptionDAO = DaoManager.createDao(this.connectionSource, ComponentOption.class);
        enumDAO = DaoManager.createDao(this.connectionSource, Enum.class);
        methodDAO = DaoManager.createDao(this.connectionSource, Method.class);
        parameterDAO = DaoManager.createDao(this.connectionSource, Parameter.class);
        propertyDAO = DaoManager.createDao(this.connectionSource, Property.class);

        TableUtils.createTableIfNotExists(this.connectionSource, ComponentGroup.class);
        TableUtils.createTableIfNotExists(this.connectionSource, ComponentGroupOption.class);
        TableUtils.createTableIfNotExists(this.connectionSource, Component.class);
        TableUtils.createTableIfNotExists(this.connectionSource, ComponentOption.class);
        TableUtils.createTableIfNotExists(this.connectionSource, Method.class);
        TableUtils.createTableIfNotExists(this.connectionSource, Parameter.class);
        TableUtils.createTableIfNotExists(this.connectionSource, Property.class);
        TableUtils.createTableIfNotExists(this.connectionSource, Enum.class);
    }

    public void removeComponentGroup(ComponentGroup componentGroup) throws SQLException {
        for (Component component : componentGroup.getComponents()) {
            for (Method method : component.getMethods()) {
                this.methodDAO.delete(method);
                this.methodDAO.commit(this.databaseConnection);
                for (Parameter parameter : method.getParameters()) {
                    this.parameterDAO.delete(parameter);
                    this.parameterDAO.commit(this.databaseConnection);
                }
            }

            for (Property property : component.getProperties()) {
                this.propertyDAO.delete(property);
                this.propertyDAO.commit(this.databaseConnection);
                for (Enum enumValue : property.getEnum()) {
                    this.enumDAO.delete(enumValue);
                    this.enumDAO.commit(this.databaseConnection);
                }
            }

            for (ComponentOption option : component.getOptions()) {
                this.componentOptionDAO.delete(option);
            }

            this.componentDAO.delete(component);
            this.componentDAO.commit(this.databaseConnection);
        }

        for (ComponentGroupOption groupOption : componentGroup.getOptions()) {
            this.componentGroupOptionDAO.delete(groupOption);
        }

        this.componentGroupDAO.delete(componentGroup);
        this.componentGroupDAO.commit(this.databaseConnection);
    }

    public void addComponentGroup(ComponentGroup componentGroup) throws SQLException {
        this.componentGroupDAO.createIfNotExists(componentGroup);
        this.componentGroupDAO.commit(this.databaseConnection);

        for (ComponentGroupOption groupOption : componentGroup.getOptions()) {
            this.componentGroupOptionDAO.createIfNotExists(groupOption);
            this.componentGroupOptionDAO.commit(this.databaseConnection);
        }

        for (Component component : componentGroup.getComponents()) {
            this.componentDAO.createIfNotExists(component);
            this.componentDAO.commit(this.databaseConnection);
            for (Method method : component.getMethods()) {
                this.methodDAO.createIfNotExists(method);
                this.methodDAO.commit(this.databaseConnection);
                for (Parameter parameter : method.getParameters()) {
                    this.parameterDAO.createIfNotExists(parameter);
                    this.parameterDAO.commit(this.databaseConnection);
                }
            }

            for (Property property : component.getProperties()) {
                this.propertyDAO.createIfNotExists(property);
                this.propertyDAO.commit(this.databaseConnection);
                for (Enum enumValue : property.getEnum()) {
                    this.enumDAO.createIfNotExists(enumValue);
                    this.enumDAO.commit(this.databaseConnection);
                }
            }

            for (ComponentOption option : component.getOptions()) {
                this.componentOptionDAO.createIfNotExists(option);
                this.componentOptionDAO.commit(this.databaseConnection);
            }
        }
    }

    public boolean existComponentGroup(ComponentGroup componentGroup) throws SQLException {
        return this.componentGroupDAO.idExists(componentGroup.getId());
    }

    public List<ComponentGroup> getComponentGroups() throws SQLException {
        return this.componentGroupDAO.queryForAll();
    }

    public void updateProperty(Property property) throws SQLException {
        this.propertyDAO.update(property);
        this.propertyDAO.commit(this.databaseConnection);
    }

    public void closeConnection() throws SQLException {
        this.connectionSource.close();
    }

}
