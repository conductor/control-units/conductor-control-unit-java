package org.conductor.core.converter;

import org.conductor.core.entities.DataType;

public final class DataTypeConverter {
    public static DataType getDataType(Object obj) throws Exception {
        switch (obj.getClass().getSimpleName()) {
            case "int":
                return DataType.INTEGER;
            case "String":
                return DataType.STRING;
            case "double":
                return DataType.DOUBLE;
            case "Boolean":
                return DataType.BOOLEAN;
            case "float":
                return DataType.FLOAT;
        }

        throw new Exception(
                "Unexpected datatype. Unable to convert datatype of type : " + obj.getClass().getSimpleName());
    }

    public static Object getObject(DataType dataType, String value) throws Exception {
        switch (dataType) {
            case INTEGER:
                return (int) Double.parseDouble(value);
            case STRING:
                return value;
            case DOUBLE:
                return Double.parseDouble(value);
            case BOOLEAN:
                return Boolean.parseBoolean(value);
            case FLOAT:
                return Float.parseFloat(value);
            case ENUM:
                return value;
        }

        throw new Exception("Unexpected datatype. Cannot convert data type: " + dataType);
    }
}
