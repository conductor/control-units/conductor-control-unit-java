package org.conductor.core.cache;

import org.conductor.core.entities.ComponentGroup;

import javax.inject.Named;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Henrik on 25/07/2016.
 */
@Named
public class ComponentGroupCache {

    private Map<String, ComponentGroup> componentGroups = new HashMap<String, ComponentGroup>();

    public void add(ComponentGroup componentGroup) {
        componentGroups.put(componentGroup.getId(), componentGroup);
    }

    public void update(ComponentGroup componentGroup) {
        componentGroups.put(componentGroup.getId(), componentGroup);
    }

    public void remove(ComponentGroup componentGroup) {
        componentGroups.remove(componentGroup);
    }

    public Collection<ComponentGroup> getAll() {
        return componentGroups.values();
    }

}
