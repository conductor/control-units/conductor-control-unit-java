package org.conductor.core.controllers;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.conductor.core.controllers.exceptions.ComponentGroupInstallationException;
import org.conductor.core.controllers.exceptions.ComponentGroupUninstallException;
import org.conductor.core.controllers.exceptions.InstallationException;
import org.conductor.core.controllers.listeners.InstallationListener;
import org.conductor.core.entities.ComponentGroup;
import org.conductor.core.facades.ComponentGroupInstallationFactory;
import org.conductor.core.facades.ComponentGroupInstaller;
import org.conductor.core.facades.InstallationStatus;
import org.conductor.core.facades.Installer;
import org.conductor.core.repository.ComponentGroupRepository;
import org.conductor.core.validation.ValidationResult;
import org.conductor.core.validation.validators.ComponentGroupValidator;

@Named
public class InstallationController {
    private static Logger log = LogManager.getLogger(InstallationController.class.getName());
    private List<Installer> installations = new ArrayList<Installer>();
    private List<InstallationListener> listeners = new ArrayList<InstallationListener>();

    @Inject
    private ComponentGroupRepository repository;

    @Inject
    private ComponentGroupInstallationFactory componentGroupInstallationFactory;

    public void installComponentGroup(ComponentGroup componentGroup) throws ComponentGroupInstallationException {
        // If the component group is null then throw exception
        if (componentGroup == null) {
            throw new ComponentGroupInstallationException(componentGroup, "Cannot install component group. Component group cannot be null.");
        }

        log.info("Installing component group '{}'.", componentGroup.getId());

        // Check that the component group contains valid data
        ValidationResult validationResult = ComponentGroupValidator.validate(componentGroup);

        if (!validationResult.isValid()) {
            throw new ComponentGroupInstallationException(componentGroup, "Component group validation failed, component group contains invalid data. " + validationResult);
        }

        // Check that the component group hasn't already been installed
        try {
            if (this.repository.existComponentGroup(componentGroup)) {
                throw new ComponentGroupInstallationException(componentGroup, "Cannot install component group. A component group already exists with the id '" + componentGroup.getId() + "'.");
            }
        } catch (SQLException e) {
            throw new ComponentGroupInstallationException(componentGroup, "Failed to install component group '" + componentGroup + "'.", e);
        }

        // Check that the component group isn't already being installed
        if (this.isComponentGroupInstallationInProgress(componentGroup)) {
            throw new ComponentGroupInstallationException(componentGroup, "A component group installation is already in progress for component group '" + componentGroup + "'.");
        }

        // Install the component group
        Installer installer = componentGroupInstallationFactory.create(componentGroup);
        installer.addInstallationListener(installationListener);
        installer.start();
        this.installations.add(installer);
    }

    public void uninstallComponentGroup(ComponentGroup componentGroup) throws ComponentGroupUninstallException {
        if (componentGroup == null) {
            throw new ComponentGroupUninstallException(componentGroup, "Cannot uninstall component group. Component group is null.");
        }

        log.info("Uninstalling component group '{}'", componentGroup.getId());

        try {
            this.repository.removeComponentGroup(componentGroup);
        } catch (Exception e) {
            throw new ComponentGroupUninstallException(componentGroup, "Failed to uninstall component group.", e);
        }
    }

    org.conductor.core.facades.InstallationListener installationListener = new org.conductor.core.facades.InstallationListener() {

        @Override
        public void onInstallationStatusChanged(Installer installer, InstallationStatus status) {
            if (status.equals(InstallationStatus.FINISHED)) {
                if (installer instanceof ComponentGroupInstaller) {
                    ComponentGroupInstaller componentGroupInstaller = (ComponentGroupInstaller) installer;

                    for (InstallationListener listener : listeners) {
                        listener.onComponentGroupInstalled(componentGroupInstaller.getComponentGroup());
                    }
                }
            }
        }

        @Override
        public void onInstallationError(InstallationException e) {
            for (InstallationListener listener : listeners) {
                listener.onInstallationFailed(e);
            }
        }

    };

    public boolean isComponentGroupInstalled(ComponentGroup componentGroup) throws SQLException {
        return repository.existComponentGroup(componentGroup);
    }

    public boolean isComponentGroupInstallationInProgress(ComponentGroup componentGroup) {
        for (Installer installer : this.installations) {
            if (installer instanceof ComponentGroupInstaller) {
                ComponentGroupInstaller componentGroupInstaller = (ComponentGroupInstaller) installer;

                if (componentGroup.getId().equals(componentGroupInstaller.getComponentGroup().getId())) {
                    return true;
                }
            }
        }

        return false;
    }

    public void addInstallationListener(InstallationListener listener) {
        listeners.add(listener);
    }

}
