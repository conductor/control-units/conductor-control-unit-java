package org.conductor.core.controllers;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Named;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.conductor.core.communicator.factory.CommunicatorFactory;
import org.conductor.core.communicator.interfaces.CommunicationChannel;
import org.conductor.core.communicator.listeners.ErrorListener;
import org.conductor.core.communicator.listeners.PropertyChangedListener;
import org.conductor.core.controllers.exceptions.MethodInvocationException;
import org.conductor.core.controllers.exceptions.PropertyInvocationException;
import org.conductor.core.converter.DataTypeConverter;
import org.conductor.core.downloader.DownloadManager;
import org.conductor.core.downloader.listeners.DownloadManagerListener;
import org.conductor.core.entities.Component;
import org.conductor.core.entities.ComponentGroup;
import org.conductor.core.entities.Method;
import org.conductor.core.entities.Parameter;
import org.conductor.core.entities.Property;
import org.conductor.core.validation.ValidationResult;
import org.conductor.core.validation.validators.ValueValidator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@Named
public class CommunicationController implements PropertyChangedListener {
    private static Logger log = LogManager.getLogger(CommunicationController.class.getName());
    private Map<ComponentGroup, CommunicationChannel> channels = new HashMap<ComponentGroup, CommunicationChannel>();
    private List<PropertyChangedListener> propertyChangedListeners = new ArrayList<PropertyChangedListener>();
    private List<ErrorListener> errorListeners = new ArrayList<ErrorListener>();
    private DownloadManager downloadManager = new DownloadManager();
    private CommunicatorFactory communicatorFactory;

    @Inject
    public CommunicationController(CommunicatorFactory communicatorFactory) {
        this.communicatorFactory = communicatorFactory;
        this.downloadManager.addEventListener(downloadListener);
    }

    public void add(ComponentGroup componentGroup) throws Exception {
        initializeCommunication(componentGroup);
    }

    private void initializeCommunication(ComponentGroup componentGroup) throws Exception {
        try {
            CommunicationChannel communicator = communicatorFactory.createCommunicator(componentGroup);
            communicator.addPropertyChangedListener(this);
            communicator.addErrorListener(componentGroupErrorListener);
            channels.put(componentGroup, communicator);
            communicator.start();
        } catch (FileNotFoundException e) {
            log.debug("Couldn't find jar file for '" + componentGroup + "'. Requesting download of jar file from repository.");
            this.downloadManager.download(componentGroup);
        }
    }

    public void setPropertyValue(Property property, Object propertyValue) throws PropertyInvocationException {
        if (property == null) {
            throw new PropertyInvocationException(property, "Failed to set property value. Property cannot be null");
        }

        log.info("Set property value '{}' of property '{}'.", propertyValue, property.getName());

        ValidationResult validationResult = ValueValidator.validate(property.getDataType(), propertyValue, property.getEnum());

        if (!validationResult.isValid()) {
            throw new PropertyInvocationException(property, "Property value validation failed. " + validationResult);
        }

        CommunicationChannel communicator = this.channels.get(property.getComponent().getComponentGroup());

        if (communicator == null) {
            throw new PropertyInvocationException(property, "The component group communication is not yet initialized.");
        }

        try {
            communicator.setPropertyValue(property, propertyValue);
        } catch (Exception e) {
            throw new PropertyInvocationException(property, "An error occured in component group communicator.", e);
        }
    }

    public void callMethod(Method method, Map<String, Object> parameters) throws MethodInvocationException {
        if (method == null) {
            throw new MethodInvocationException(method, "Cannot call method. Method cannot be null.");
        }

        log.info("Calling method {}", method.getName());

        try {
            Collection<Parameter> declaredParameters = method.getParameters();
            for (Parameter parameter : declaredParameters) {
                if (!parameters.containsKey(parameter.getName())) {
                    throw new MethodInvocationException(method, "Cannot call method '" + method.getName() + "'. The parameter '" + parameter.getName() + "' is missing.");
                }

                Object parameterValue = parameters.get(parameter.getName());
                try {
                    parameterValue = DataTypeConverter.getObject(parameter.getDataType(), parameterValue.toString()); // We need to convert the parameter because JSON can't differ between integer and double as every number thats gets to the client gets converted to double...
                } catch (Exception e) {
                    throw new MethodInvocationException(method, "Failed to convert parameter value.", e);
                }
                parameters.put(parameter.getName(), parameterValue);

                ValidationResult validationResult = ValueValidator.validate(parameter.getDataType(), parameterValue);

                if (!validationResult.isValid()) {
                    throw new MethodInvocationException(method, "Method parameter validation failed. " + validationResult);
                }
            }

            CommunicationChannel communicator = this.channels.get(method.getComponent().getComponentGroup());
            communicator.callMethod(method, parameters);
        } catch (MethodInvocationException e) {
            throw e;
        } catch (Exception e) {
            throw new MethodInvocationException(method, "Failed to invoke method.", e);
        }
    }

    @Override
    public void onPropertyChanged(Property property, Object propertyValue) {
        ValidationResult validationResult = ValueValidator.validate(property.getDataType(), propertyValue);

        if (!validationResult.isValid()) {
            for (ErrorListener listener : errorListeners) {
                listener.onComponentError(property.getComponent(), "Received an invalid property value from component group. " + validationResult, null);
            }

            return;
        }

        for (PropertyChangedListener listener : propertyChangedListeners) {
            listener.onPropertyChanged(property, propertyValue);
        }
    }

    public void addPropertyChangedListener(PropertyChangedListener listener) {
        propertyChangedListeners.add(listener);
    }

    public void addErrorListener(ErrorListener listener) {
        errorListeners.add(listener);
    }

    ErrorListener componentGroupErrorListener = new ErrorListener() {

        @Override
        public void onComponentError(Component component, String errorMessage, String stackTrace) {
            for (ErrorListener listener : errorListeners) {
                listener.onComponentError(component, errorMessage, stackTrace);
            }
        }

        @Override
        public void onComponentGroupError(ComponentGroup componentGroup, String errorMessage, String stackTrace) {
            for (ErrorListener listener : errorListeners) {
                listener.onComponentGroupError(componentGroup, errorMessage, stackTrace);
            }
        }
    };

    DownloadManagerListener downloadListener = new DownloadManagerListener() {

        @Override
        public void onDownloadFinished(ComponentGroup componentGroup) {
            try {
                initializeCommunication(componentGroup);
            } catch (Exception e) {
                log.error("An error occured while initializing component group communication.", e);
            }
        }

        @Override
        public void onDownloadError(ComponentGroup componentGroup, Exception e) {
            for (ErrorListener listener : errorListeners) {
                listener.onComponentGroupError(componentGroup, "Failed to download component group '" + componentGroup + "'. " + e.getMessage(), ExceptionUtils.getStackTrace(e));
            }
        }

    };
}
