package org.conductor.core.controllers.exceptions;

import org.conductor.core.entities.Method;

public class MethodInvocationException extends Exception {
    private Method method;
    private Throwable cause = null;

    public MethodInvocationException(Method method, String message) {
        this(method, message, null);
    }

    public MethodInvocationException(Method method, String message, Throwable cause) {
        super(message);
        this.method = method;
        this.cause = cause;
    }

    public Throwable getCause() {
        return cause;
    }

    public void printStackTrace() {
        super.printStackTrace();
        if (cause != null) {
            System.err.println("Caused by:");
            cause.printStackTrace();
        }
    }

    public void printStackTrace(java.io.PrintStream ps) {
        super.printStackTrace(ps);
        if (cause != null) {
            ps.println("Caused by:");
            cause.printStackTrace(ps);
        }
    }

    public void printStackTrace(java.io.PrintWriter pw) {
        super.printStackTrace(pw);
        if (cause != null) {
            pw.println("Caused by:");
            cause.printStackTrace(pw);
        }
    }

    public Method getMethod() {
        return this.method;
    }
}
