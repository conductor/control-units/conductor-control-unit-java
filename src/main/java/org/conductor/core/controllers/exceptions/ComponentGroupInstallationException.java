package org.conductor.core.controllers.exceptions;

import org.conductor.core.entities.ComponentGroup;

public class ComponentGroupInstallationException extends InstallationException {
    private ComponentGroup componentGroup;
    private Throwable cause = null;

    public ComponentGroupInstallationException(ComponentGroup componentGroup, String message) {
        this(componentGroup, message, null);
    }

    public ComponentGroupInstallationException(ComponentGroup componentGroup, String message, Throwable cause) {
        super(message, cause);
        this.componentGroup = componentGroup;
        this.cause = cause;
    }

    public Throwable getCause() {
        return cause;
    }

    public void printStackTrace() {
        super.printStackTrace();
        if (cause != null) {
            System.err.println("Caused by:");
            cause.printStackTrace();
        }
    }

    public void printStackTrace(java.io.PrintStream ps) {
        super.printStackTrace(ps);
        if (cause != null) {
            ps.println("Caused by:");
            cause.printStackTrace(ps);
        }
    }

    public void printStackTrace(java.io.PrintWriter pw) {
        super.printStackTrace(pw);
        if (cause != null) {
            pw.println("Caused by:");
            cause.printStackTrace(pw);
        }
    }

    public ComponentGroup getComponentGroup() {
        return this.componentGroup;
    }
}
