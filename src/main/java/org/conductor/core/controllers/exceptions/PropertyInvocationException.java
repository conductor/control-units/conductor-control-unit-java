package org.conductor.core.controllers.exceptions;

import org.conductor.core.entities.Property;

public class PropertyInvocationException extends Exception {
    private Property property;
    private Throwable cause = null;

    public PropertyInvocationException(Property property, String message) {
        this(property, message, null);
    }

    public PropertyInvocationException(Property property, String message, Throwable cause) {
        super(message);
        this.property = property;
        this.cause = cause;
    }

    public Throwable getCause() {
        return cause;
    }

    public void printStackTrace() {
        super.printStackTrace();
        if (cause != null) {
            System.err.println("Caused by:");
            cause.printStackTrace();
        }
    }

    public void printStackTrace(java.io.PrintStream ps) {
        super.printStackTrace(ps);
        if (cause != null) {
            ps.println("Caused by:");
            cause.printStackTrace(ps);
        }
    }

    public void printStackTrace(java.io.PrintWriter pw) {
        super.printStackTrace(pw);
        if (cause != null) {
            pw.println("Caused by:");
            cause.printStackTrace(pw);
        }
    }

    public Property getProperty() {
        return this.property;
    }
}
