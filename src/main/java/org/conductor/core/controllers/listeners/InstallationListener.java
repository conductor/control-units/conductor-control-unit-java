package org.conductor.core.controllers.listeners;

import org.conductor.core.controllers.exceptions.InstallationException;
import org.conductor.core.entities.ComponentGroup;

public interface InstallationListener {
    void onComponentGroupInstalled(ComponentGroup componentGroup);

    void onInstallationFailed(InstallationException installationException);
}
