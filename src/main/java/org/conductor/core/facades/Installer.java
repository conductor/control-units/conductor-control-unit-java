package org.conductor.core.facades;

import java.util.ArrayList;
import java.util.List;

import org.conductor.core.controllers.exceptions.InstallationException;

public abstract class Installer {
    private InstallationStatus status = InstallationStatus.WAITING;
    private List<InstallationListener> listeners = new ArrayList<InstallationListener>();

    public abstract void start();

    public void changeStatus(InstallationStatus status) {
        this.status = status;

        for (InstallationListener listener : this.listeners) {
            listener.onInstallationStatusChanged(this, status);
        }
    }

    public void errorHandler(InstallationException e) {
        for (InstallationListener listener : this.listeners) {
            listener.onInstallationError(e);
        }
    }

    public InstallationStatus getStatus() {
        return this.status;
    }

    public void addInstallationListener(InstallationListener listener) {
        this.listeners.add(listener);
    }

    public void removeInstallationListener(InstallationListener listener) {
        this.listeners.remove(listener);
    }
}
