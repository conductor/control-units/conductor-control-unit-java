package org.conductor.core.facades;

public enum InstallationStatus {
    WAITING,
    STARTED,
    FINISHED,
    FAILED
}
