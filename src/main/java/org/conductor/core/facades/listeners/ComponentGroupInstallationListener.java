package org.conductor.core.facades.listeners;

import org.conductor.core.controllers.exceptions.InstallationException;
import org.conductor.core.entities.ComponentGroup;

public interface ComponentGroupInstallationListener {
    void onComponentGroupInstalled(ComponentGroup componentGroup);

    void onComponentGroupInstallationError(InstallationException e);
}
