package org.conductor.core.facades;

import javax.inject.Inject;

import org.conductor.core.downloader.DownloadManager;
import org.conductor.core.entities.ComponentGroup;
import org.conductor.core.repository.ComponentGroupImplementationRepository;
import org.conductor.core.repository.ComponentGroupRepository;
import org.springframework.stereotype.Component;

@Component
public class ComponentGroupInstallationFactory {
    private DownloadManager downloadManager;
    private ComponentGroupRepository repository;
    private ComponentGroupImplementationRepository componentGroupImplementationRepository;

    @Inject
    public ComponentGroupInstallationFactory(DownloadManager downloadManager, ComponentGroupRepository componentGroupRepository, ComponentGroupImplementationRepository componentGroupImplementationRepository) {
        this.downloadManager = downloadManager;
        this.repository = componentGroupRepository;
        this.componentGroupImplementationRepository = componentGroupImplementationRepository;
    }

    public Installer create(ComponentGroup componentGroup) {
        return new ComponentGroupInstaller(downloadManager, repository, componentGroupImplementationRepository, componentGroup);
    }
}
