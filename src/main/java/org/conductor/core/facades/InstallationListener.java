package org.conductor.core.facades;

import org.conductor.core.controllers.exceptions.InstallationException;

public interface InstallationListener {
    void onInstallationStatusChanged(Installer installer, InstallationStatus status);

    void onInstallationError(InstallationException e);
}
