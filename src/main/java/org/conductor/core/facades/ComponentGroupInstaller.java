package org.conductor.core.facades;

import java.net.MalformedURLException;
import java.sql.SQLException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.conductor.core.controllers.exceptions.ComponentGroupInstallationException;
import org.conductor.core.downloader.DownloadManager;
import org.conductor.core.downloader.listeners.DownloadManagerListener;
import org.conductor.core.entities.ComponentGroup;
import org.conductor.core.repository.ComponentGroupImplementationRepository;
import org.conductor.core.repository.ComponentGroupRepository;

public class ComponentGroupInstaller extends Installer {
    private static Logger log = LogManager.getLogger(ComponentGroupInstaller.class.getName());
    private DownloadManager downloadManager;
    private ComponentGroupRepository repository;
    private ComponentGroupImplementationRepository componentGroupImplementationRepository;
    private ComponentGroup componentGroup;

    public ComponentGroupInstaller(DownloadManager downloadManager, ComponentGroupRepository repository, ComponentGroupImplementationRepository componentGroupImplementationRepository, ComponentGroup componentGroup) {
        this.downloadManager = downloadManager;
        this.repository = repository;
        this.componentGroupImplementationRepository = componentGroupImplementationRepository;
        this.componentGroup = componentGroup;
        this.downloadManager.addEventListener(downloadListener);
    }

    public void start() {
        changeStatus(InstallationStatus.STARTED);

        try {
            if (this.componentGroupImplementationRepository.exist(componentGroup)) {
                installationFinished();
            } else {
                this.downloadManager.download(componentGroup);
            }
        } catch (MalformedURLException e) {
            ComponentGroupInstallationException installationException = new ComponentGroupInstallationException(this.componentGroup, "Failed to install component group '" + componentGroup + "'.", e);
            errorHandler(installationException);
        }
    }

    private void installationFinished() {
        try {
            this.repository.addComponentGroup(componentGroup);
        } catch (SQLException e) {
            ComponentGroupInstallationException installationException = new ComponentGroupInstallationException(this.componentGroup, "Failed to install component group '" + componentGroup + "'.", e);
            errorHandler(installationException);
        }

        changeStatus(InstallationStatus.FINISHED);
    }

    DownloadManagerListener downloadListener = new DownloadManagerListener() {

        @Override
        public void onDownloadFinished(ComponentGroup downloadComponenetGroup) {
            if (downloadComponenetGroup == componentGroup) {
                installationFinished();
            }
        }

        @Override
        public void onDownloadError(ComponentGroup downloadComponenetGroup, Exception e) {
            if (downloadComponenetGroup == componentGroup) {
                ComponentGroupInstallationException installationException = new ComponentGroupInstallationException(componentGroup, "Failed to install component group '" + componentGroup + "'.", e);
                errorHandler(installationException);
            }
        }
    };

    public ComponentGroup getComponentGroup() {
        return this.componentGroup;
    }

}
