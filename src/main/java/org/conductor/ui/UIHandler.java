package org.conductor.ui;

import javax.swing.JFrame;
import javax.swing.JTabbedPane;

public class UIHandler {
    JFrame mainFrame;

    public UIHandler() {
        this.mainFrame = new JFrame("Conductor Client");

        JTabbedPane tabbedPane = new JTabbedPane();

        this.mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.mainFrame.setContentPane(tabbedPane);
        this.mainFrame.pack();
        this.mainFrame.setVisible(true);
    }
}
