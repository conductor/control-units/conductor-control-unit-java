package org.conductor.bootstrap;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Main {
    private static Logger log = LogManager.getLogger(Main.class.getName());

    static {
        try {
            //If this is a linux system then load the linux driver for sqlite
            String os = System.getProperty("os.name");
            log.info("OS detected: {}", os);
            if (os.contains("nix") || os.contains("nux") || os.contains("aix")) {
                log.info("Loading libsqlitejdbc.so");
                System.load("/lib/libsqlitejdbc.so");
                log.info("libsqlitejdbc.so loaded successfully");
            }
        } catch (UnsatisfiedLinkError e) {
            log.error("Native code library failed to load.\n" + e);
            System.exit(1);
        }
    }

    public static void main(String[] args) throws Exception {
        log.info("Starting application.");

        try {
            ApplicationContext context = new ClassPathXmlApplicationContext(new String[]{"Spring-AutoScan.xml"});

            BootstrapApplication bootstrap = (BootstrapApplication) context.getBean("bootstrapApplication");
            bootstrap.start();
        } catch (Exception e) {
            log.error("Failed to start application.", e);
        }
    }
}
