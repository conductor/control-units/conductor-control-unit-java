package org.conductor.bootstrap;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.conductor.core.cache.ComponentGroupCache;
import org.conductor.core.controllers.CommunicationController;
import org.conductor.core.entities.ComponentGroup;
import org.conductor.core.repository.ComponentGroupRepository;
import org.conductor.integration.client.Conductor;
import org.conductor.pairing.PairingServer;
import org.conductor.properties.PropertyHandler;
import org.conductor.ui.UIHandler;
import org.springframework.beans.factory.annotation.Autowired;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.Collection;

@Named
public class BootstrapApplication {

    private static Logger log = LogManager.getLogger(BootstrapApplication.class.getName());

    @Inject
    private Conductor conductor;

    @Inject
    private PropertyHandler propertyHandler;

    @Autowired
    private ComponentGroupRepository repository;

    @Inject
    private ComponentGroupCache componentGroupCache;

    @Inject
    private CommunicationController communicationController;

    public void start() throws Exception {
        Collection<ComponentGroup> componentGroups = repository.getComponentGroups();
        for (ComponentGroup componentGroup : componentGroups) {
            componentGroupCache.add(componentGroup);
            communicationController.add(componentGroup);
        }

        if (getServerIp() == null || getPortNumber() == null || getApiKey() == null || getBasePath() == null) {
            log.warn("Server ip, port, base path and/or apiKey isn't set in the properties file.");
        } else {
            conductor.connect(getServerIp(), getPortNumber(), getBasePath(), getApiKey(), useSSL());
            conductor.autoReconnectToServer(true);
        }

        new PairingServer();
        new UIHandler();
    }

    private String getServerIp() {
        return propertyHandler.get("serverIp");
    }

    private Integer getPortNumber() {
        String portNumber = propertyHandler.get("port");

        if (portNumber == null) {
            return null;
        }

        return Integer.parseInt(portNumber);
    }

    private String getBasePath() {
        return propertyHandler.get("serverBasePath");
    }

    private boolean useSSL() {
        String useSSL = propertyHandler.get("serverUseSSL");

        if (useSSL == null) {
            return false;
        }

        return Boolean.parseBoolean(useSSL);
    }

    private String getApiKey() {
        return propertyHandler.get("apiKey");
    }
}
