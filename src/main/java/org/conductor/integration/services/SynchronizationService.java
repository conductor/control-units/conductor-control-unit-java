package org.conductor.integration.services;

import org.conductor.integration.client.listeners.ConnectionListener;
import org.springframework.stereotype.Service;

@Service
public class SynchronizationService implements ConnectionListener {
    @Override
    public void onConnected() {

    }
    /*
	private static Logger log = LogManager.getLogger(SynchronizationService.class.getName());
	private Conductor conductor;
	private InstallationController controller;
	private DeviceCache deviceCache;

	@Inject
	public SynchronizationService(Conductor conductor, InstallationController controller, DeviceCache deviceCache) {
		this.conductor = conductor;
		this.controller = controller;
		this.deviceCache = deviceCache;
		
		conductor.addConnectionListener(this);
	}

	@Override
	public void onConnected() {
		log.info("Conductor client is connected to the server! Starting synchronization.");
		getDevices(new DeviceResult() {

			@Override
			public void result(List<Device> devices) {
				log.info("Synchronizing devices and components.");
				syncInstallation(devices);
				log.info("Synchronizing property values.");
				syncPropertyValues();
				log.info("Synchronization completed!");
			}

		});
	}

	public void getDevices(DeviceResult callback) {
		DDPListener ddpListener = new DDPListener() {
			@Override
			public void onResult(Map<String, Object> resultFields) {
				try {
					if (resultFields.containsKey(DdpMessageField.ERROR)) {
						Map<String, Object> error = (Map<String, Object>) resultFields.get(DdpMessageField.ERROR);
						String errorReason = (String) error.get("reason");
						throw new Exception(errorReason);
					} else {
						Gson gson = new GsonBuilder().setPrettyPrinting().create();
						log.info(gson.toJson(resultFields));
						Map<String, Object> result = (Map<String, Object>) resultFields.get("result");
						List<Map<String, Object>> rawDevices = (List<Map<String, Object>>) result.get("devices");
						List<Device> devices = new ArrayList<Device>();

						for (Map<String, Object> rawDevice : rawDevices) {
							log.info(rawDevice);
							Device device = DeviceTranslator.translate(rawDevice);
							devices.add(device);
						}

						callback.result(devices);
					}
				} catch (Exception e) {
					log.error("An error occured while fetching installed devices from the server.", e);
					conductor.sendErrorReport(null, "An error occured while fetching installed devices from the server. " + e.getMessage(), null);
				}
			}
		};

		log.info("Fetching devices from conductor server.");
		this.conductor.getDevices(ddpListener);
	}
	
	private void syncInstallation(List<Device> devices) {
		try {
			// Uninstall devices
			Collection<Device> existingDevices = deviceCache.getAll();
			List<Device> removedDevices = getDeviceDifference(existingDevices, devices);

			for (Device device : removedDevices) {
				controller.uninstallDevice(device);
			}

			// Uninstall component groups
			for (Device device : devices) {
				Device existingDevice = FilterUtil.getDevice(existingDevices, device.getId());
				List<ComponentGroup> removedComponentGroups = getComponentGroupDifference(existingDevice.getComponentGroups(), device.getComponentGroups());

				for (ComponentGroup componentGroup : removedComponentGroups) {
					controller.uninstallComponentGroup(componentGroup);
				}
			}

			// Install devices
			for (Device device : devices) {
				if(!controller.isDeviceInstalled(device) && !controller.isDeviceInstallationInProgress(device)) {
					controller.installDevice(device);
				}
			}

			// Install component groups
			for (Device device : devices) {
				for (ComponentGroup componentGroup : device.getComponentGroups()) {
					if(!controller.isDeviceInstalled(device) && !controller.isDeviceInstallationInProgress(device) && !controller.isComponentGroupInstalled(componentGroup) && !controller.isComponentGroupInstallationInProgress(componentGroup)) {
						controller.installComponentGroup(device, componentGroup);
					}
				}
			}
		} catch (DeviceInstallationException e) {
			log.error("An error occured while synchronizing installations with the server. Installation failed for device '{}'.", e.getDevice().getId(), e);
			conductor.sendErrorReport(null, e.getDevice().getId(), "An error occured while installing. " + e.getMessage(), ExceptionUtils.getStackTrace(e));
		} catch (ComponentGroupInstallationException e) {
			log.error("An error occured while synchronizing installations with the server. Installation failed for component group '{}'.", e.getComponentGroup().getId(), e);
			conductor.sendErrorReport(null, e.getComponentGroup().getDevice().getId(), e.getComponentGroup().getId(), "An error occured while installing. " + e.getMessage(), ExceptionUtils.getStackTrace(e));
		} catch (DeviceUninstallException e) {
			log.error("An error occured while synchronizing installations with the server. Uninstallation failed for device '{}'.", e.getDevice().getId(), e);
			conductor.sendErrorReport(null, e.getDevice().getId(), "An error occured while uninstalling. " + e.getMessage(), ExceptionUtils.getStackTrace(e));
		} catch (ComponentGroupUninstallException e) {
			log.error("An error occured while synchronizing installations with the server. Uninstallation failed for component group '{}'.", e.getComponentGroup().getId(), e);
			conductor.sendErrorReport(null, e.getComponentGroup().getDevice().getId(), e.getComponentGroup().getId(), "An error occured while uninstalling. " + e.getMessage(), ExceptionUtils.getStackTrace(e));
		} catch (SQLException e) {
			log.error("An error occured while synchronizing with the server.", e);
			conductor.sendErrorReport(null, "An error occured while synchronizing installations with the server. " + e.getMessage(), ExceptionUtils.getStackTrace(e));
		}
	}

	private void syncPropertyValues() {
//		try {
//			List<Device> devices = deviceRepository.getDevices();
//
//			for (Device device : devices) {
//				for (ComponentGroup componentGroup : device.getComponentGroups()) {
//					for (org.conductor.core.entities.Component component : componentGroup.getComponents()) {
//						for (Property property : component.getProperties()) {
//							if (!property.isSynced) {
//								conductor.onPropertyValueUpdated(property);
//								this.conductor.setPropertyValue(device.getId(), component.getId(), property.getName(), property.getValue(), listener);
//							}
//						}
//					}
//				}
//			}
//		} catch (SQLException e) {
//			log.error("Failed to synchronize property values with the conductor server. An error occured while fetching devices from the database.", e);
//			conductor.sendErrorReport(null, "Failed to synchronize property values with the conductor server. An error occured while fetching devices from the database. " + e.getMessage(), ExceptionUtils.getStackTrace(e));
//		}
	}

	private List<Device> getDeviceDifference(Collection<Device> devices, Collection<Device> otherDevices) {
		List<Device> difference = new ArrayList<Device>();

		for (Device device : devices) {
			boolean found = false;

			for (Device otherDevice : otherDevices) {
				if (device.getId().equals(otherDevice.getId())) {
					found = true;
				}
			}

			if (!found) {
				difference.add(device);
			}
		}

		return difference;
	}

	private List<ComponentGroup> getComponentGroupDifference(Collection<ComponentGroup> componentGroups, Collection<ComponentGroup> otherComponentGroups) {
		List<ComponentGroup> difference = new ArrayList<ComponentGroup>();

		for (ComponentGroup componentGroup : componentGroups) {
			boolean found = false;

			for (ComponentGroup otherComponentGroup : otherComponentGroups) {
				if (componentGroup.getId().equals(otherComponentGroup.getId())) {
					found = true;
				}
			}

			if (!found) {
				difference.add(componentGroup);
			}
		}

		return difference;
	}
	*/
}
