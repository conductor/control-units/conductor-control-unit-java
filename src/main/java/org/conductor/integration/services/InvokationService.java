package org.conductor.integration.services;

import java.util.Collection;
import java.util.Map;

import javax.inject.Inject;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.conductor.core.cache.ComponentGroupCache;
import org.conductor.core.communicator.listeners.ErrorListener;
import org.conductor.core.communicator.listeners.PropertyChangedListener;
import org.conductor.core.controllers.CommunicationController;
import org.conductor.core.controllers.exceptions.MethodInvocationException;
import org.conductor.core.controllers.exceptions.PropertyInvocationException;
import org.conductor.core.entities.Component;
import org.conductor.core.entities.ComponentGroup;
import org.conductor.core.entities.DataType;
import org.conductor.core.entities.Method;
import org.conductor.core.entities.Property;
import org.conductor.core.util.FilterUtil;
import org.conductor.integration.client.Conductor;
import org.conductor.integration.client.listeners.InvokeListener;
import org.springframework.stereotype.Service;

import com.keysolutions.ddpclient.DDPClient.DdpMessageField;
import com.keysolutions.ddpclient.DDPListener;

@Service
public class InvokationService implements InvokeListener {
    private static Logger log = LogManager.getLogger(InvokationService.class.getName());
    private Conductor conductor;
    private CommunicationController controller;
    private ComponentGroupCache componentGroupCache;

    @Inject
    public InvokationService(Conductor conductor, CommunicationController controller, ComponentGroupCache componentGroupCache) {
        this.conductor = conductor;
        this.controller = controller;
        this.componentGroupCache = componentGroupCache;

        this.conductor.addInvokeListener(this);
        this.controller.addPropertyChangedListener(propertyChangedListener);
        this.controller.addErrorListener(errorListener);
    }

    @Override
    public void onSetPropertyValue(String requestId, Map<String, Object> data) {
        String componentId = (String) data.get("componentId");
        String propertyName = (String) data.get("propertyName");
        Object propertyValue = data.get("propertyValue");

        Collection<ComponentGroup> componentGroups = componentGroupCache.getAll();
        Property property = FilterUtil.getProperty(componentGroups, componentId, propertyName);

        // JSON can't distinguish between integer, double and float values. So we need to convert the value to the correct data type before we send it of to the component group.
        if (property.getDataType().equals(DataType.INTEGER) || property.getDataType().equals(DataType.FLOAT) || property.getDataType().equals(DataType.DOUBLE)) {
            propertyValue = jsonNumberFix(property.getDataType(), propertyValue);
        }

        try {
            this.controller.setPropertyValue(property, propertyValue);
            this.conductor.sendOKResponse(requestId);
        } catch (PropertyInvocationException e) {
            log.error("Failed to set property value '{}' in property '{}' in component '{}'.", propertyValue, propertyName, componentId, e);
            this.conductor.sendErrorResponse(requestId, "Failed to set property value. " + e.getMessage(), ExceptionUtils.getStackTrace(e));
        }
    }

    private Object jsonNumberFix(DataType dataType, Object propertyValue) {
        switch (dataType) {
            case DOUBLE:
                return propertyValue;
            case INTEGER:
                return ((Double) propertyValue).intValue();
            case FLOAT:
                return ((Double) propertyValue).floatValue();
            default:
                return propertyValue;
        }
    }

    @Override
    public void onCallMethod(String requestId, Map<String, Object> data) {
        String componentId = (String) data.get("componentId");
        String methodName = (String) data.get("methodName");

        Collection<ComponentGroup> componentGroups = componentGroupCache.getAll();
        Method method = FilterUtil.getMethod(componentGroups, componentId, methodName);

        try {
            Map<String, Object> parameters = (Map<String, Object>) data.get("methodParameters");

            this.controller.callMethod(method, parameters);
            this.conductor.sendOKResponse(requestId);
        } catch (MethodInvocationException e) {
            log.error("Failed to invoke method '{}' in component '{}'.", methodName, componentId, e);
            this.conductor.sendErrorResponse(requestId, "Failed to invoke method. " + e.getMessage(), ExceptionUtils.getStackTrace(e));
        }
    }

    PropertyChangedListener propertyChangedListener = new PropertyChangedListener() {

        @Override
        public void onPropertyChanged(Property property, Object propertyValue) {
            log.info("Property '{}' in component '{}' in component group '{}' updated value '{}'", property.getName(), property.getComponent().getName(), property.getComponent().getComponentGroup().getId(), propertyValue);
            Component component = property.getComponent();

            try {
                conductor.setPropertyValue(component.getId(), property.getName(), propertyValue, new DDPListener() {
                    @Override
                    public void onResult(Map<String, Object> resultFields) {
                        if (resultFields.containsKey(DdpMessageField.ERROR)) {
                            Map<String, Object> error = (Map<String, Object>) resultFields.get(DdpMessageField.ERROR);
                            String errorReason = (String) error.get("reason");
                            log.error("Error when uppdating property value | reason: " + errorReason);
                        } else {
                            log.info("Property value synced with remote server.");
                        }
                    }
                });
            } catch (Exception e) {
                log.error("Failed to send the updated property value to the server.", e);
            }
        }

    };

    ErrorListener errorListener = new ErrorListener() {

        @Override
        public void onComponentError(Component component, String errorMessage, String stackTrace) {
            log.error("A component ({}) in component group '{}' reported an error. {}", component.getId(), component.getComponentGroup(), errorMessage, stackTrace);
            ComponentGroup componentGroup = component.getComponentGroup();
            conductor.sendErrorReport(componentGroup.getId(), component.getId(), errorMessage, stackTrace);
        }

        @Override
        public void onComponentGroupError(ComponentGroup componentGroup, String errorMessage, String stackTrace) {
            log.error("A component group ({}) reported an error. {}", componentGroup, errorMessage, stackTrace);
            conductor.sendErrorReport(componentGroup.getId(), errorMessage, stackTrace);
        }

    };
}
