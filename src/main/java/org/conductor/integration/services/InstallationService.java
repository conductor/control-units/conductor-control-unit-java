package org.conductor.integration.services;

import java.util.Collection;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Named;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.conductor.core.cache.ComponentGroupCache;
import org.conductor.core.controllers.CommunicationController;
import org.conductor.core.controllers.InstallationController;
import org.conductor.core.controllers.exceptions.ComponentGroupInstallationException;
import org.conductor.core.controllers.exceptions.InstallationException;
import org.conductor.core.entities.ComponentGroup;
import org.conductor.core.util.FilterUtil;
import org.conductor.integration.client.Conductor;
import org.conductor.integration.client.listeners.InstallationListener;
import org.conductor.integration.translators.ComponentGroupTranslator;

@Named
public class InstallationService implements InstallationListener {
    private static Logger log = LogManager.getLogger(InstallationService.class.getName());
    private Conductor conductor;
    private InstallationController installationController;
    private CommunicationController communicationController;
    private ComponentGroupCache componentGroupCache;

    @Inject
    public InstallationService(Conductor conductor, InstallationController installationController, CommunicationController communicationController, ComponentGroupCache componentGroupCache) {
        this.installationController = installationController;
        this.communicationController = communicationController;
        this.conductor = conductor;
        this.componentGroupCache = componentGroupCache;

        this.installationController.addInstallationListener(installationListener);
        this.conductor.addInstallationListener(this);
    }

    @Override
    public void onInstallComponentGroup(String requestId, Map<String, Object> data) {
        try {
            Map<String, Object> rawComponentGroup = (Map<String, Object>) data.get("componentGroup");
            ComponentGroup componentGroup = ComponentGroupTranslator.translate(rawComponentGroup);
            componentGroup.setInstallationRequestId(requestId);
            this.installationController.installComponentGroup(componentGroup);
        } catch (Exception e) {
            log.error("Failed to install component group '{}'.", data, e);
            this.conductor.sendErrorResponse(requestId, "Failed to install component group. " + e.getMessage(), ExceptionUtils.getStackTrace(e));
        }
    }

    @Override
    public void onUninstallComponentGroup(String requestId, String componentGroupId) {
        Collection<ComponentGroup> componentGroups = componentGroupCache.getAll();
        ComponentGroup componentGroup = FilterUtil.getComponentGroup(componentGroups, componentGroupId);

        try {
            this.installationController.uninstallComponentGroup(componentGroup);
            this.conductor.sendOKResponse(requestId);
        } catch (Exception e) {
            log.error("Failed to uninstall component group '{}'.", componentGroupId, e);
            this.conductor.sendErrorResponse(requestId, "Failed to uninstall component group. " + e.getMessage(), ExceptionUtils.getStackTrace(e));
        }
    }

    org.conductor.core.controllers.listeners.InstallationListener installationListener = new org.conductor.core.controllers.listeners.InstallationListener() {

        @Override
        public void onComponentGroupInstalled(ComponentGroup componentGroup) {
            log.info("Component group '{}' was successfully installed!", componentGroup);
            conductor.sendOKResponse(componentGroup.getInstallationRequestId());
            try {
                componentGroupCache.add(componentGroup);
                communicationController.add(componentGroup);
            } catch (Exception e) {
                log.error("Failed to setup communication with component group '{}'.", componentGroup, e);
                conductor.sendErrorReport(componentGroup.getId(), e.getMessage(), ExceptionUtils.getStackTrace(e));
            }
        }

        @Override
        public void onInstallationFailed(InstallationException installationException) {
            if (installationException instanceof ComponentGroupInstallationException) {
                ComponentGroupInstallationException e = (ComponentGroupInstallationException) installationException;
                log.error("Component group '{}' installation failed.", e.getComponentGroup(), e);
                conductor.sendErrorResponse(e.getComponentGroup().getInstallationRequestId(), e.getMessage(), ExceptionUtils.getStackTrace(e));
            }
        }
    };
}
