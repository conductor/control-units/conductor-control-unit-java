package org.conductor.integration.client.listeners;

import java.util.Map;

public interface InstallationListener {
    void onInstallComponentGroup(String requestId, Map<String, Object> rawComponentGroup);

    void onUninstallComponentGroup(String requestId, String componentGroupId);
}
