package org.conductor.integration.client.listeners;

public interface ConnectionListener {
    void onConnected();
}
