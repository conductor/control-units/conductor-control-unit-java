package org.conductor.integration.client.listeners;

import java.util.Map;

public interface InvokeListener {
    void onSetPropertyValue(String requestId, Map<String, Object> data);

    void onCallMethod(String requestId, Map<String, Object> data);
}
