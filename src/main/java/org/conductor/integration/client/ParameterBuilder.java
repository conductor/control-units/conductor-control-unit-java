package org.conductor.integration.client;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Henrik on 04/08/2016.
 */
public class ParameterBuilder {

    private List<Object> parameters = new ArrayList<Object>();

    public ParameterBuilder add(Object obj) {
        parameters.add(obj);
        return this;
    }

    public Object[] build() {
        return parameters.toArray(new Object[parameters.size()]);
    }

}
