package org.conductor.integration.client;

import org.conductor.core.entities.Property;

public interface UpdatePropertyResponse {
    void onResponse(boolean success, Property property);
}
