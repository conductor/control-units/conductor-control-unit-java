package org.conductor.integration.client;

import org.apache.commons.lang3.StringUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Henrik on 25/07/2016.
 */
public enum Collections {
    SET_PROPERTY_VALUE_REQUESTS("setPropertyValueRequests"),
    INSTALL_COMPONENT_GROUP_REQUESTS("installComponentGroupRequests"),;

    private static Map<String, Collections> namesMap = new HashMap<String, Collections>();
    private final String text;

    Collections(final String text) {
        this.text = text;
    }

    static {
        namesMap.put("setPropertyValueRequests", SET_PROPERTY_VALUE_REQUESTS);
        namesMap.put("installComponentGroupRequests", INSTALL_COMPONENT_GROUP_REQUESTS);
    }

    public static Collections forValue(String value) {
        return namesMap.get(StringUtils.lowerCase(value));
    }

    @Override
    public String toString() {
        return this.text;
    }
}
