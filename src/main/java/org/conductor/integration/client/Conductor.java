package org.conductor.integration.client;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.KeyStore;
import java.security.cert.Certificate;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.conductor.bootstrap.BootstrapApplication;
import org.conductor.integration.client.listeners.ConnectionListener;
import org.conductor.integration.client.listeners.InstallationListener;
import org.conductor.integration.client.listeners.InvokeListener;
import org.springframework.stereotype.Component;

import com.keysolutions.ddpclient.DDPClient;
import com.keysolutions.ddpclient.DDPListener;

import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;

@Component
public class Conductor extends DDPClient implements Observer {
    private static Logger log = LogManager.getLogger(Conductor.class.getName());
    private String serverIp;
    private int portNumber;
    private String apiKey;
    private Thread reconnectDDPThread;
    private Thread heartbeatThread;
    private List<InstallationListener> installationListeners = new ArrayList<>();
    private List<InvokeListener> invokeListeners = new ArrayList<>();
    private List<ConnectionListener> connectionListeners = new ArrayList<>();

    public Conductor() throws URISyntaxException {
        this.addObserver(this);
    }

    public void connect(String serverIp, int portNumber, String basePath, String apiKey, boolean useSSL) throws URISyntaxException {
        this.apiKey = apiKey;
        this.serverIp = serverIp;
        this.portNumber = portNumber;

        log.info("Connecting to '" + this.serverIp + ":" + this.portNumber + basePath + "' using api key '" + apiKey + "'");

        if (useSSL) {
            super.connect(serverIp, portNumber, basePath, getTrustManagers());
        } else {
            super.connect(serverIp, portNumber, basePath, false);
        }

        if (this.heartbeatThread == null) {
            this.heartbeatThread = new Thread(new HeartbeatThread(this));
            this.heartbeatThread.start();
        }
    }

    public void autoReconnectToServer(boolean autoReconnect) {
        if (reconnectDDPThread != null) {
            reconnectDDPThread.interrupt();
        }

        if (autoReconnect) {
            reconnectDDPThread = new Thread(new ReconnectDDP(this));
            reconnectDDPThread.start();
        }
    }

    private class ReconnectDDP implements Runnable {
        private DDPClient client;

        public ReconnectDDP(DDPClient client) {
            this.client = client;
        }

        public void run() {
            while (!Thread.currentThread().isInterrupted()) {
                if (client.getState() == CONNSTATE.Disconnected || client.getState() == CONNSTATE.Closed) {
                    log.info("Reconnecting to server");
                    client.connect();
                }

                try {
                    Thread.sleep(30000);
                } catch (InterruptedException e) {
                    log.error("Failed to sleep thread.", e);
                }
            }
        }
    }

    private void onConnected() {
        doSubscription();

        for (ConnectionListener listener : connectionListeners) {
            listener.onConnected();
        }

        sendHeartbeat();
    }

    private void doSubscription() {
        Map<String, Object> parameter = new HashMap<>();
        parameter.put("apiKey", apiKey);

        Object[] parameters = new Object[1];
        parameters[0] = parameter;

        this.subscribe(Publications.INSTALL_COMPONENT_GROUP_REQUESTS.toString(), parameters);
        this.subscribe(Publications.SET_PROPERTY_VALUE_REQUESTS.toString(), parameters);
    }

    public void update(Observable client, Object msg) {
        log.debug("Update message received from conductor server. Messages '{}'.", msg);

        if (msg instanceof Map<?, ?>) {
            Map<String, Object> jsonFields = (Map<String, Object>) msg;

            String msgType = (String) jsonFields.get(DDPClient.DdpMessageField.MSG);

            if (msgType == null) {
                log.error("Received message where msg field was null. {}", msg);
                return;
            }

            switch (msgType) {
                case DdpMessageType.ADDED:
                    String collectionName = (String) jsonFields.get(DdpMessageField.COLLECTION);
                    String documentId = (String) jsonFields.get(DdpMessageField.ID);
                    Map<String, Object> fields = (Map<String, Object>) jsonFields.get(DdpMessageField.FIELDS);

                    log.info("New document ({}) for collection '{}' and values '{}'.", documentId, collectionName, fields);
                    onDocumentAdded(collectionName, documentId, fields);
                    break;
                case DdpMessageType.CONNECTED:
                    log.info("Connection to the server established!");
                    onConnected();
                    break;
                case DdpMessageType.ERROR:
                    String errorMsg = (String) jsonFields.get(DdpMessageField.ERRORMSG);
                    log.error("Received error message '{}' from conductor server.", errorMsg);
                    break;
                case DdpMessageType.CLOSED:
                    log.info("Connection to conductor server was closed.");
                    break;
                case DdpMessageType.CONNECT:
                    log.info("Connecting to conductor server.");
                    break;
            }
        }
    }

    private void onDocumentAdded(String collectionName, String documentId, Map<String, Object> fields) {
        try {
            log.info("Received document for collection {} with id {} and fields {}.", collectionName, documentId, fields);

            if (collectionName.equals(Collections.SET_PROPERTY_VALUE_REQUESTS.toString())) {
                log.info("Received a set property value request.");

                for (InvokeListener listener : invokeListeners) {
                    listener.onSetPropertyValue(documentId, fields);
                }
            } else if (collectionName.equals(Collections.INSTALL_COMPONENT_GROUP_REQUESTS.toString())) {
                log.info("Received a install component group request.");

                for (InstallationListener listener : installationListeners) {
                    listener.onInstallComponentGroup(documentId, fields);
                }
            } else {
                log.error("Received a message with an unknown collection from the server.");
                throw new Exception("Received a message with a unknown collection (" + collectionName + ").");
            }
        } catch (Exception ex) {
            log.error("{}", ex);
        }
    }

    protected class HeartbeatThread implements Runnable {
        private DDPClient client;

        public HeartbeatThread(DDPClient client) {
            this.client = client;
        }

        @Override
        public void run() {
            while (!Thread.currentThread().isInterrupted()) {
                if (client.getState().equals(CONNSTATE.Connected)) {
                    sendHeartbeat();
                }

                try {
                    Thread.sleep(30000); // Send a heartbeat to the server every 30 seconds
                } catch (InterruptedException e) {
                    log.error("Failed to sleep thread.", e);
                }
            }
        }

    }

    private void sendHeartbeat() {
        log.info("Sending heartbeat");

        Object[] parameters = new ParameterBuilder()
                .add(apiKey)
                .build();

        this.call("ControlUnit.heartbeat", parameters);
    }

    public void setPropertyValue(String componentId, String propertyName, Object propertyValue, DDPListener listener) {
        log.info("Sending property value to server");

        Object[] parameters = new ParameterBuilder()
                .add(apiKey)
                .add(componentId)
                .add(propertyName)
                .add(propertyValue)
                .build();

        this.call("ControlUnit.updatePropertyValue", parameters, listener);
    }

    public void sendOKResponse(String requestId) {
        log.info("Sending OK response to the server");

        Object[] parameters = new ParameterBuilder()
                .add(apiKey)
                .add(requestId)
                .build();

        this.call("ControlUnit.okResponse", parameters, new DDPListener() {
            @Override
            public void onResult(Map<String, Object> resultFields) {
                if (resultFields.containsKey(DdpMessageField.ERROR)) {
                    Map<String, Object> error = (Map<String, Object>) resultFields.get(DdpMessageField.ERROR);
                    String errorReason = (String) error.get("reason");
                    log.error("Error when sending ok response to server | reason: " + errorReason);
                }
            }
        });
    }

    public void sendErrorResponse(String requestId, String errorMessage, String stackTrace) {
        log.info("Sending ERROR response to the server");

        Object[] parameters = new ParameterBuilder()
                .add(apiKey)
                .add(requestId)
                .add(errorMessage)
                .add(stackTrace)
                .build();

        this.call("ControlUnit.errorResponse", parameters, new DDPListener() {
            @Override
            public void onResult(Map<String, Object> resultFields) {
                if (resultFields.containsKey(DdpMessageField.ERROR)) {
                    Map<String, Object> error = (Map<String, Object>) resultFields.get(DdpMessageField.ERROR);
                    String errorReason = (String) error.get("reason");
                    log.error("Error when sending ok response to server | reason: " + errorReason);
                }
            }
        });
    }

    public void sendErrorReport(String errorMessage, String stackTrace) {
        sendErrorReport(null, null, errorMessage, stackTrace);
    }

    public void sendErrorReport(String componentGroupId, String errorMessage, String stackTrace) {
        sendErrorReport(componentGroupId, null, errorMessage, stackTrace);
    }

    public void sendErrorReport(String componentGroupId, String componentId, String errorMessage, String stackTrace) {
        log.info("Sending error report to server");

        Object[] parameters = new ParameterBuilder()
                .add(apiKey)
                .add(componentGroupId)
                .add(componentId)
                .add(errorMessage)
                .add(stackTrace)
                .build();

        this.call("ControlUnit.errorReport", parameters, new DDPListener() {
            @Override
            public void onResult(Map<String, Object> resultFields) {
                if (resultFields.containsKey(DdpMessageField.ERROR)) {
                    Map<String, Object> error = (Map<String, Object>) resultFields.get(DdpMessageField.ERROR);
                    String errorReason = (String) error.get("reason");
                    log.error("Error when sending error report to server | reason: " + errorReason);
                } else {
                    log.info("Error report sent successfully to server");
                }
            }
        });
    }

    public void addInstallationListener(InstallationListener listener) {
        installationListeners.add(listener);
    }

    public void removeInstallationListener(InstallationListener listener) {
        installationListeners.remove(listener);
    }

    public void addInvokeListener(InvokeListener listener) {
        invokeListeners.add(listener);
    }

    public void removeInvokeListener(InvokeListener listener) {
        invokeListeners.remove(listener);
    }

    public void addConnectionListener(ConnectionListener listener) {
        connectionListeners.add(listener);
    }

    public void removeConnectionListener(ConnectionListener listener) {
        connectionListeners.remove(listener);
    }

    private TrustManager[] getTrustManagers() {
        try {
            KeyStore keyStore = KeyStore.getInstance(KeyStore.getDefaultType());
            Path ksPath = Paths.get(System.getProperty("java.home"), "lib", "security", "cacerts");
            keyStore.load(Files.newInputStream(ksPath), "changeit".toCharArray());
            CertificateFactory cf = CertificateFactory.getInstance("X.509");

            try (InputStream caInput = new BufferedInputStream(
                    BootstrapApplication.class.getClassLoader().getResourceAsStream("certificates/letsencrypt.der"))) {
                Certificate crt = cf.generateCertificate(caInput);
                log.info("Added Cert for " + ((X509Certificate) crt).getSubjectDN());
                keyStore.setCertificateEntry("DSTRootCAX3", crt);
            }

            TrustManagerFactory tmf = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
            tmf.init(keyStore);
            return tmf.getTrustManagers();
        } catch (Exception e) {
            log.error("Failed to get trust managers.", e);
            throw new RuntimeException(e);
        }
    }

}