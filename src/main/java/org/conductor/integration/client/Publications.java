package org.conductor.integration.client;

import org.apache.commons.lang3.StringUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Henrik on 25/07/2016.
 */
public enum Publications {
    SET_PROPERTY_VALUE_REQUESTS("command.set_property_value_requests"),
    SET_PROPERTY_VALUE_RESPONSES("command.set_property_value_responses"),
    INSTALL_COMPONENT_GROUP_REQUESTS("command.install_component_group_requests"),
    INSTALL_COMPONENT_GROUP_RESPONSES("command.install_component_group_responses");

    private static Map<String, Publications> namesMap = new HashMap<String, Publications>();
    private final String text;

    Publications(final String text) {
        this.text = text;
    }

    static {
        namesMap.put("command.set_property_value_requests", SET_PROPERTY_VALUE_REQUESTS);
        namesMap.put("command.set_property_value_responses", SET_PROPERTY_VALUE_RESPONSES);
        namesMap.put("command.install_component_group_requests", INSTALL_COMPONENT_GROUP_REQUESTS);
        namesMap.put("command.install_component_group_responses", INSTALL_COMPONENT_GROUP_RESPONSES);
    }

    public static Publications forValue(String value) {
        return namesMap.get(StringUtils.lowerCase(value));
    }

    @Override
    public String toString() {
        return this.text;
    }

}
