package org.conductor.integration.translators;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.conductor.core.entities.Component;
import org.conductor.core.entities.ComponentGroup;
import org.conductor.core.entities.ComponentGroupOption;

public class ComponentGroupTranslator {
    public static ComponentGroup translate(Map<String, Object> data) throws Exception {
        ComponentGroup componentGroup = new ComponentGroup();
        componentGroup.setVersion((String) data.get("version"));
        componentGroup.setId((String) data.get("id"));
        componentGroup.setName((String) data.get("name"));
        componentGroup.setOrg((String) data.get("org"));
        componentGroup.setRepositoryUrl((String) data.get("repositoryUrl"));

        List<Map<String, Object>> dataOptions = (List<Map<String, Object>>) data.get("options");
        Collection<ComponentGroupOption> groupOptions = new ArrayList<ComponentGroupOption>();

        for (Map<String, Object> dataOption : dataOptions) {
            ComponentGroupOption option = new ComponentGroupOption();
            option.setName((String) dataOption.get("name"));
            option.setDataType(DataTypeTranslator.translate((String) dataOption.get("dataType")));
            option.setValue(dataOption.get("value"));
            option.setComponentGroup(componentGroup);
            groupOptions.add(option);
        }

        componentGroup.setOptions(groupOptions);

        List<Map<String, Object>> dataComponents = (List<Map<String, Object>>) data.get("components");
        Collection<Component> components = new ArrayList<Component>();

        for (Map<String, Object> dataComponent : dataComponents) {
            Component component = ComponentTranslator.translate(dataComponent);
            component.setComponentGroup(componentGroup);
            components.add(component);
        }

        componentGroup.setComponents(components);
        return componentGroup;
    }

}
