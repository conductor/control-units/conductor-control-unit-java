package org.conductor.integration.translators;

import org.conductor.core.entities.DataType;

public class DataTypeTranslator {
    public static DataType translate(String dataType) throws Exception {
        switch (dataType) {
            case "string":
                return DataType.STRING;
            case "integer":
                return DataType.INTEGER;
            case "double":
                return DataType.DOUBLE;
            case "float":
                return DataType.FLOAT;
            case "enum":
                return DataType.ENUM;
            case "boolean":
                return DataType.BOOLEAN;
        }

        throw new Exception("Invalid data type. Cannot convert '" + dataType + "'");
    }
}
