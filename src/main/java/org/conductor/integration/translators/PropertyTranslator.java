package org.conductor.integration.translators;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.conductor.core.entities.Enum;
import org.conductor.core.entities.Property;

public class PropertyTranslator {
    public static Collection<Property> translateAll(List<Map<String, Object>> dataProperties) throws Exception {
        Collection<Property> properties = new ArrayList<Property>();

        for (Map<String, Object> dataProperty : dataProperties) {
            Property property = new Property();
            property.setName(dataProperty.get("name").toString());
            property.setDataType(DataTypeTranslator.translate((String) dataProperty.get("dataType")));
            property.setValue(dataProperty.get("value"));
            property.setEnum(translateEnums((List<String>) dataProperty.get("enum")));
            property.setSynced(true);
            properties.add(property);
        }

        return properties;
    }

    private static Collection<Enum> translateEnums(List<String> dataEnums) {
        if (dataEnums == null) {
            return new ArrayList<>();
        }

        Collection<Enum> enums = new ArrayList<>();

        for (String dataEnum : dataEnums) {
            Enum enumValue = new Enum(dataEnum);
            enums.add(enumValue);
        }

        return enums;
    }
}
