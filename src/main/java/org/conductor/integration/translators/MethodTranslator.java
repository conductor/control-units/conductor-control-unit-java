package org.conductor.integration.translators;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.conductor.core.entities.DataType;
import org.conductor.core.entities.Method;
import org.conductor.core.entities.Parameter;

public class MethodTranslator {
    public static Collection<Method> translateAll(List<Map<String, Object>> dataMethods) throws Exception {
        Collection<Method> methods = new ArrayList<Method>();

        for (Map<String, Object> dataMethod : dataMethods) {
            Method method = new Method();
            method.setName(dataMethod.get("name").toString());

            Collection<Parameter> parameters = translateMethodParameters((Map<String, Object>) dataMethod.get("parameters"));
            for (Parameter parameter : parameters) {
                parameter.setMethod(method);
            }

            method.setParameters(parameters);
            methods.add(method);
        }

        return methods;
    }

    private static Collection<Parameter> translateMethodParameters(Map<String, Object> dataParameters) throws Exception {
        Collection<Parameter> parameters = new ArrayList<Parameter>();

        for (Map.Entry<String, Object> dataParameter : dataParameters.entrySet()) {
            Parameter parameter = new Parameter();
            parameter.setName(dataParameter.getKey());
            DataType dataType = getDataType((String) dataParameter.getValue());
            parameter.setDataType(dataType);
            parameters.add(parameter);
        }

        return parameters;
    }

    private static DataType getDataType(String dataType) throws Exception {
        switch (dataType) {
            case "string":
                return DataType.STRING;
            case "integer":
                return DataType.INTEGER;
            case "double":
                return DataType.DOUBLE;
            case "float":
                return DataType.FLOAT;
            case "enum":
                return DataType.ENUM;
            case "boolean":
                return DataType.BOOLEAN;
        }

        throw new Exception("Invalid data type. Unable to convert data type: " + dataType);
    }
}
