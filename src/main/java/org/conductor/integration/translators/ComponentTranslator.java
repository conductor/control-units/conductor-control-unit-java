package org.conductor.integration.translators;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.conductor.core.entities.Component;
import org.conductor.core.entities.ComponentOption;
import org.conductor.core.entities.Method;
import org.conductor.core.entities.Property;

public class ComponentTranslator {
    public static Component translate(Map<String, Object> dataComponent) throws Exception {
        Component component = new Component();
        component.setId((String) dataComponent.get("id"));
        component.setName((String) dataComponent.get("name"));

        List<Map<String, Object>> dataOptions = (List<Map<String, Object>>) dataComponent.get("options");
        Collection<ComponentOption> componentOptions = new ArrayList<ComponentOption>();

        for (Map<String, Object> dataOption : dataOptions) {
            ComponentOption option = new ComponentOption();
            option.setName((String) dataOption.get("name"));
            option.setDataType(DataTypeTranslator.translate((String) dataOption.get("dataType")));
            option.setValue(dataOption.get("value"));
            option.setComponent(component);
            componentOptions.add(option);
        }

        component.setOptions(componentOptions);

        List<Map<String, Object>> dataProperties = (List<Map<String, Object>>) dataComponent.get("properties");
        Collection<Property> properties = PropertyTranslator.translateAll(dataProperties);

        for (Property property : properties) {
            property.setComponent(component);
        }
        component.setProperties(properties);

        List<Map<String, Object>> dataMethods = (List<Map<String, Object>>) dataComponent.get("methods");
        Collection<Method> methods = MethodTranslator.translateAll(dataMethods);

        for (Method method : methods) {
            method.setComponent(component);
        }

        component.setMethods(methods);

        return component;
    }
}
