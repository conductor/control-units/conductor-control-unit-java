package org.conductor.pairing;

import java.io.IOException;

import org.conductor.pairing.handlers.AcceptPairingHandler;
import org.conductor.pairing.handlers.NotFoundHandler;
import org.conductor.pairing.handlers.RequestPairingHandler;
import org.glassfish.grizzly.http.server.HttpServer;

public class PairingServer {
    private int PORT_NUMBER = 8001;

    public PairingServer() throws IOException {
        CSRFHandler csrfHandler = new CSRFHandler();

        HttpServer server = HttpServer.createSimpleServer(null, "localhost", PORT_NUMBER);
        server.getServerConfiguration().addHttpHandler(new RequestPairingHandler(csrfHandler), "/pairing/request");
        server.getServerConfiguration().addHttpHandler(new AcceptPairingHandler(csrfHandler, server), "/pairing/accept");
        server.getServerConfiguration().addHttpHandler(new NotFoundHandler());
        server.start();
    }
}
