package org.conductor.pairing.handlers;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.conductor.core.util.ResourceUtil;
import org.glassfish.grizzly.http.server.HttpHandler;
import org.glassfish.grizzly.http.server.Request;
import org.glassfish.grizzly.http.server.Response;
import org.stringtemplate.v4.ST;

import java.io.IOException;

/**
 * Created by Henrik on 03/12/2017.
 */
public class NotFoundHandler extends HttpHandler {
    private static Logger log = LogManager.getLogger(RequestPairingHandler.class.getName());

    @Override
    public void service(Request request, Response response) throws Exception {
        log.info("Received HTTP request on invalid endpoint '{}?{}'.", request.getRequestURL(), request.getQueryString());

        String templateFile = ResourceUtil.getResourceFile(this, "templates/notFound.st");
        ST template = new ST(templateFile, '$', '$');
        sendOKResponse(response, template.render());
    }

    private void sendOKResponse(Response response, String message) throws IOException {
        log.info("Sending OK response. ", message);
        response.setContentType("text/html");
        response.setContentLength(message.length());
        response.getWriter().write(message);
        response.getWriter().flush();
        response.getWriter().close();
    }

}
