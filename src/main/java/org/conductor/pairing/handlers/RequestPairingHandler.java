package org.conductor.pairing.handlers;

import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.conductor.core.util.ResourceUtil;
import org.conductor.pairing.CSRFHandler;
import org.stringtemplate.v4.ST;

import org.glassfish.grizzly.http.server.HttpHandler;
import org.glassfish.grizzly.http.server.Request;
import org.glassfish.grizzly.http.server.Response;

public class RequestPairingHandler extends HttpHandler {
    private static Logger log = LogManager.getLogger(RequestPairingHandler.class.getName());
    private CSRFHandler csrfHandler;

    public RequestPairingHandler(CSRFHandler csrfHandler) {
        this.csrfHandler = csrfHandler;
    }

    @Override
    public void service(Request request, Response response) throws Exception {
        log.info("Received HTTP pairing request '{}?{}'.", request.getRequestURL(), request.getQueryString());
        String templateFile = ResourceUtil.getResourceFile(this, "templates/pairingRequest.st");

        try {
            validateHasQueryParameters(request, "ip", "port", "basePath", "useSSL", "apiKey");
        } catch (Exception e) {
            sendErrorResponse(response, e.getMessage());
            return;
        }

        ST template = new ST(templateFile, '$', '$');
        template.add("localPort", request.getLocalPort());
        template.add("ip", request.getParameter("ip"));
        template.add("port", request.getParameter("port"));
        template.add("basePath", request.getParameter("basePath"));
        template.add("useSSL", request.getParameter("useSSL"));
        template.add("apiKey", request.getParameter("apiKey"));
        template.add("csrfToken", this.csrfHandler.getToken());

        sendOKResponse(response, template.render());
    }

    private void validateHasQueryParameters(Request request, String... parameterNames) throws Exception {
        for (String parameterName : parameterNames) {
            if (request.getParameter(parameterName) == null || request.getParameter(parameterName).equals("")) {
                throw new Exception("Missing query parameter '" + parameterName + "'.");
            }
        }
    }

    private void sendErrorResponse(Response response, String message) throws IOException {
        log.info("Sending error response. ", message);

        String templateFile = ResourceUtil.getResourceFile(this, "templates/error.st");
        ST template = new ST(templateFile, '$', '$');
        template.add("message", message);
        String responseMessage = template.render();

        response.setContentType("text/html");
        response.setContentLength(responseMessage.length());
        response.getWriter().write(responseMessage);
        response.getWriter().flush();
        response.getWriter().close();
    }

    private void sendOKResponse(Response response, String message) throws IOException {
        response.setContentType("text/html");
        response.setContentLength(message.length());
        response.getWriter().write(message);
        response.getWriter().flush();
        response.getWriter().close();
    }

}
