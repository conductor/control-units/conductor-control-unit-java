package org.conductor.pairing.handlers;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.StringJoiner;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.conductor.core.util.ResourceUtil;
import org.conductor.pairing.CSRFHandler;
import org.conductor.properties.PropertyHandler;
import org.glassfish.grizzly.http.server.HttpHandler;
import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.grizzly.http.server.Request;
import org.glassfish.grizzly.http.server.Response;
import org.stringtemplate.v4.ST;

public class AcceptPairingHandler extends HttpHandler {
    private static Logger log = LogManager.getLogger(AcceptPairingHandler.class.getName());
    private CSRFHandler csrfHandler;
    private HttpServer server;

    public AcceptPairingHandler(CSRFHandler csrfHandler, HttpServer server) {
        this.csrfHandler = csrfHandler;
        this.server = server;
    }

    @Override
    public void service(Request request, Response response) throws Exception {
        log.info("Received HTTP accept pairing request '{}?{}'.", request.getRequestURL(), request.getQueryString());
        String token = request.getParameter("csrfToken");

        if (!this.csrfHandler.isTokenValid(token)) {
            log.info("Token is invalid, sending error response to client.");
            sendErrorResponse(response, "Your token has expired. Please try again.");
            return;
        }

        try {
            validateHasQueryParameters(request, "ip", "port", "basePath", "useSSL", "apiKey");
        } catch (Exception e) {
            sendErrorResponse(response, e.getMessage());
            return;
        }

        PropertyHandler.getInstance().set("serverIp", request.getParameter("ip"));
        PropertyHandler.getInstance().set("port", request.getParameter("port"));
        PropertyHandler.getInstance().set("serverBasePath", request.getParameter("basePath"));
        PropertyHandler.getInstance().set("serverUseSSL", request.getParameter("useSSL"));
        PropertyHandler.getInstance().set("apiKey", request.getParameter("apiKey"));

        sendOKResponse(response);

        log.info("Shutting down pairing server.");
        this.server.shutdownNow();

        if (isRunningInDebug()) {
            log.info("Exiting application.");
            System.exit(0);
        } else {
            try {
                log.info("Restarting application.");
                restartApplication();
            } catch (Exception e) {
                log.error("Failed to restart application.", e);
                sendErrorResponse(response, "Failed to restart application.");
                throw e;
            }
        }
    }

    private void validateHasQueryParameters(Request request, String... parameterNames) throws Exception {
        for (String parameterName : parameterNames) {
            if (request.getParameter(parameterName) == null || request.getParameter(parameterName).equals("")) {
                throw new Exception("Missing query parameter '" + parameterName + "'.");
            }
        }
    }

    private void restartApplication() throws URISyntaxException, IOException {
        String javaBin = System.getProperty("java.home") + File.separator + "bin" + File.separator + "java";
        String applicationFilePath = getApplicationPath();

        StringJoiner command = new StringJoiner(" ")
                .add(javaBin)
                .add("-jar")
                .add(applicationFilePath);

        log.info("Restarting application '" + command.toString() + "'.");
        Runtime.getRuntime().exec(command.toString());

        log.info("Exiting application.");
        System.exit(0);
    }

    private void sendErrorResponse(Response response, String message) throws IOException {
        log.info("Sending error response. ", message);

        String templateFile = ResourceUtil.getResourceFile(this, "templates/error.st");
        ST template = new ST(templateFile, '$', '$');
        template.add("message", message);
        String responseMessage = template.render();

        response.setContentType("text/html");
        response.setContentLength(responseMessage.length());
        response.getWriter().write(responseMessage);
        response.getWriter().flush();
        response.getWriter().close();
    }

    private void sendOKResponse(Response response) throws IOException {
        log.info("Sending OK response.");

        String templateFile = ResourceUtil.getResourceFile(this, "templates/pairingSuccess.st");
        ST template = new ST(templateFile, '$', '$');
        String message = template.render();

        response.setContentType("text/html");
        response.setContentLength(message.length());
        response.getWriter().write(message);
        response.getWriter().flush();
        response.getWriter().close();
    }

    private boolean isRunningInDebug() {
        String file = AcceptPairingHandler.class.getProtectionDomain().getCodeSource().getLocation().getFile();
        return !file.endsWith(".jar");
    }

    private String getApplicationPath() throws MalformedURLException, URISyntaxException {
        String file = AcceptPairingHandler.class.getProtectionDomain().getCodeSource().getLocation().getFile();
        file = file.substring(0, file.lastIndexOf('!'));
        return new File(new URI(file)).getPath();
    }

}
