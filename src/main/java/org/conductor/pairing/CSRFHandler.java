package org.conductor.pairing;

import java.util.UUID;

public class CSRFHandler {
    private UUID csrfToken = UUID.randomUUID();

    public boolean isTokenValid(String csrfToken) {
        return this.getToken().equals(csrfToken);
    }

    public String getToken() {
        return csrfToken.toString();
    }
}
