package org.conductor.properties;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Properties;

import javax.inject.Named;

@Named
public class PropertyHandler {
    private static PropertyHandler propertyHandler;
    private Properties properties;
    private File propertiesFile;

    private PropertyHandler() throws IOException {
        propertiesFile = new File("./config.properties");

        if (!propertiesFile.exists()) {
            propertiesFile.createNewFile();
        }

        FileInputStream inputStream = new FileInputStream(propertiesFile);

        properties = new Properties();
        properties.load(inputStream);
    }

    public static PropertyHandler getInstance() throws IOException {
        if (propertyHandler == null) {
            propertyHandler = new PropertyHandler();
        }

        return propertyHandler;
    }

    public String get(String name) {
        return properties.getProperty(name);
    }

    public void set(String name, String value) throws IOException {
        properties.setProperty(name, value);
        OutputStream out = new FileOutputStream(this.propertiesFile);
        properties.store(out, null);
    }
}
