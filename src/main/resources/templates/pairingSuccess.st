<html>
    <head>
        <title>Pairing Request</title>
        <style>
            body {
                background-color: #EEEEEE;
                text-align: center;
                font-family: 'Roboto', 'Helvetica', 'Arial', sans-serif;
                color: #333;
            }

            .well {
                background-color: #fff;
                padding: 19px;
                -webkit-box-shadow: 0 8px 17px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
                box-shadow: 0 8px 17px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
                border-radius: 2px;
                border: 0;
                margin: 20px;
            }

            h1 {
                font-weight: 300;
            }

            p {
                font-size: 14px;
            }
        </style>
    </head>
    <body>
        <div class="well">
            <h1>Success!</h1>
            <p>Your control unit has been successfully paired. You can now close this window.</p>
        </div>
    </body>
</html>