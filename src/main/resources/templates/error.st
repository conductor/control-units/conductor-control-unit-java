<html>
    <head>
        <title>Pairing Request</title>
        <style>
            body {
                background-color: #EEEEEE;
                text-align: center;
                font-family: 'Roboto', 'Helvetica', 'Arial', sans-serif;
                color: #333;
            }

            .well {
                background-color: #fff;
                padding: 19px;
                -webkit-box-shadow: 0 8px 17px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
                box-shadow: 0 8px 17px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
                border-radius: 2px;
                border: 0;
                margin: 20px;
            }

            h1 {
                font-weight: 300;
            }

            p {
                font-size: 14px;
            }

            .btn.btn-primary {
                box-shadow: 0 2px 2px 0 rgba(0, 0, 0, 0.14), 0 3px 1px -2px rgba(0, 0, 0, 0.2), 0 1px 5px 0 rgba(0, 0, 0, 0.12);
                background-color: #009688;
                color: rgba(255,255,255, 0.84);
                border: none;
                border-radius: 2px;
                position: relative;
                padding: 8px 30px;
                margin: 10px 1px;
                font-size: 14px;
                font-weight: 500;
                text-transform: uppercase;
                letter-spacing: 0;
                will-change: box-shadow, transform;
                transition: box-shadow 0.2s cubic-bezier(0.4, 0, 1, 1), background-color 0.2s cubic-bezier(0.4, 0, 0.2, 1), color 0.2s cubic-bezier(0.4, 0, 0.2, 1);
                outline: 0;
                cursor: pointer;
                text-decoration: none;
                display: inline-block;
                line-height: 1.42857143;
                text-align: center;
                white-space: nowrap;
                vertical-align: middle;
            }
        </style>
    </head>
    <body>
        <div class="well">
            <h1>An error occurred</h1>
            <p>$message$</p>
        </div>
    </body>
</html>