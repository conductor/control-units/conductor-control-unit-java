# Java Control Unit For Conductor
Control unit written in Java used for running component groups.

# Build Instructions
- Git clone project "https://gitlab.com/conductor/control-units/utilities/java-ddp-client".
- Run "gradle clean fatJar" in java-ddp-client folder.
- Copy the file java-ddp-client/build/libs/java-ddp-client-all-1.0.0.3.jar and paste it in conductor-control-unit-java/libs.
- Run "gradle clean fatJar" in conductor-control-unit-java folder.
- Go to the folder conductor-control-unit-java/build/libs. 
- Add a property file called "config.properties" and add the server ip and port number to Conductor and your control unit private api key (see example below).
- Run "java -jar conductor-control-unit-java-all-1.0.jar".

# Configuration Example
```
serverIp=localhost
port=3030
apiKey=c2e1299f-6147-476b-903e-86f0f09afae5
```